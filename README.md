Contributions are welcome! Please read the sections below, then open a Merge Request. SMART data is required.

Please open an issue if you notice any mistakes, or have a suggestion or question.

# Folder structure
- [Protocol]
  - [Storage Type]
    - [Manufacturer]
      - [Model Family]
        - [Model Name/Number]
          - [Serial Number]
            - SMART.json
            - SMART.txt
            - ReadSpeed (optional)
              - RS###.txt
              - [Any other [ReadSpeed](https://www.grc.com/readspeed.htm)-related files]
            - SpinTest (optional)
              - [Any files related to SpinRite test releases]
            - SMART Archive (optional)
              - SMART - yyyy-MM-dd HH-mm.json
              - SMART - yyyy-MM-dd HH-mm.txt
            - HD Tune (optional)
              - [Any [HD Tune](https://www.hdtune.com/) files]

# Gathering SMART data
* Download and/or install [smartmontools](https://www.smartmontools.org) 7.0 or later.
   * Windows files: https://sourceforge.net/projects/smartmontools/files/
      * You can [extract the contents of the EXE](https://www.smartmontools.org/wiki/Download#InstalltheWindowspackage) with 7-Zip if you don't want to install it.
* [Update the drive database](https://www.smartmontools.org/wiki/Download#Updatethedrivedatabase).
  * `update-smart-drivedb`
  * You should probably update it every month or so.
* Find the drive you want to work with.
  * `smartctl --scan`
  * I will use `/dev/sda` in the following examples. The command above may return a different identifier for you.
* Gather identifying information.
  * `smartctl -i /dev/sda`
* Create the necessary folders using the information from the previous command.
  * You may have to use information from the label or look it up online, especially with SSDs.
  * If you can't find something like the Model Family or Model Number, use `Unknown`.
    * If you're not sure whether something should be the Model Family or the Model Number, take a guess based on the specificity. For example, `X1` is vague, so it's probably a Model Family.
  * For the Serial Number, I usually use what smartctl returns. The main exception is when I remove characters from the front to better match the label (Western Digital HDDs are a notable example).
    * For Hitachi HDDs, I remove the first 6 digits if they are present so all drives have a consistent format.
* Save the SMART data to files.
  * `smartctl -x /dev/sda > SMART.txt`
  * `smartctl -jx /dev/sda > SMART.json`
* If you want to gather SMART data multiple times, please move old files into a `SMART Archive` folder and rename them following the format in #Folder Structure.

# Creating a Merge Request
* Create a gitlab.com account, or sign in.
  * https://gitlab.com/users/sign_in
* Create a fork.
  * https://gitlab.com/ColbyBouma/Storage-Media-Data/-/forks/new
  * https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
* Clone your fork to your computer.
  * If you want to just use the web interface, skip this step.
  * https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository
* Add your directories and files.
  * https://docs.gitlab.com/ee/gitlab-basics/add-file.html
  * https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file
* Commit and push your changes.
  * If you are using the web interface, it creates a commit for you and pushing is unecessary.
  * https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-and-commit-local-changes
* Create a Merge Request.
  * https://gitlab.com/Gamuholic/Storage-Media-Data/-/merge_requests/new
  * https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html