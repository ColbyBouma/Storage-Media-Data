smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.11.2-pmagic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Vendor:               HP
Product:              EG0300FBDSP
Revision:             HPD6
Compliance:           SPC-3
User Capacity:        300,000,000,000 bytes [300 GB]
Logical block size:   512 bytes
Rotation Rate:        10025 rpm
Form Factor:          2.5 inches
Logical Unit id:      0x50000393c8285194
Serial number:        ECA1PC10CP2L1203
Device type:          disk
Transport protocol:   SAS (SPL-3)
Local Time is:        Wed Sep 15 02:53:23 2021 CDT
SMART support is:     Available - device has SMART capability.
SMART support is:     Enabled
Temperature Warning:  Enabled
Read Cache is:        Enabled
Writeback Cache is:   Disabled

=== START OF READ SMART DATA SECTION ===
SMART Health Status: OK

Current Drive Temperature:     35 C
Drive Trip Temperature:        65 C

Manufactured in week 03 of year 2012
Specified cycle count over device lifetime:  50000
Accumulated start-stop cycles:  23
Specified load-unload count over device lifetime:  200000
Accumulated load-unload cycles:  0
Elements in grown defect list: 0

Error counter log:
           Errors Corrected by           Total   Correction     Gigabytes    Total
               ECC          rereads/    errors   algorithm      processed    uncorrected
           fast | delayed   rewrites  corrected  invocations   [10^9 bytes]  errors
read:          0        1         0         0          0    2378469.088           0
write:         0        0         0         0          0     207995.222           0

Non-medium error count:      178

SMART Self-test log
Num  Test              Status                 segment  LifeTime  LBA_first_err [SK ASC ASQ]
     Description                              number   (hours)
# 1  Background long   Completed                   -   55879                 - [-   -    -]
# 2  Background short  Completed                   -   55873                 - [-   -    -]
# 3  Background short  Completed                   -       7                 - [-   -    -]
# 4  Background short  Completed                   -       2                 - [-   -    -]

Long (extended) Self-test duration: 2960 seconds [49.3 minutes]

Background scan results log
  Status: scan is active
    Accumulated power on time, hours:minutes 55879:55 [3352795 minutes]
    Number of background scans performed: 2013,  scan progress: 82.89%
    Number of background medium scans performed: 0

Protocol Specific port log page for SAS SSP
relative target port id = 1
  generation code = 2
  number of phys = 1
  phy identifier = 0
    attached device type: SAS or SATA device
    attached reason: unknown
    reason: loss of dword synchronization
    negotiated logical link rate: phy enabled; 6 Gbps
    attached initiator port: ssp=1 stp=1 smp=1
    attached target port: ssp=0 stp=0 smp=0
    SAS address = 0x50000393c8285196
    attached SAS address = 0x590b11c0190c6e01
    attached phy identifier = 5
    Invalid DWORD count = 4
    Running disparity error count = 4
    Loss of DWORD synchronization = 1
    Phy reset problem = 0
    Phy event descriptors:
     Invalid word count: 4
     Running disparity error count: 4
     Loss of dword synchronization count: 1
     Phy reset problem count: 0
     Elasticity buffer overflow count: 0
     Received abandon-class OPEN_REJECT count: 0
     Transmitted BREAK count: 0
     Received BREAK count: 0
     Transmitted SSP frame error count: 0
     Received SSP frame error count: 0
relative target port id = 2
  generation code = 2
  number of phys = 1
  phy identifier = 1
    attached device type: no device attached
    attached reason: unknown
    reason: unknown
    negotiated logical link rate: phy enabled; unknown
    attached initiator port: ssp=0 stp=0 smp=0
    attached target port: ssp=0 stp=0 smp=0
    SAS address = 0x50000393c8285197
    attached SAS address = 0x0
    attached phy identifier = 0
    Invalid DWORD count = 0
    Running disparity error count = 0
    Loss of DWORD synchronization = 0
    Phy reset problem = 0
    Phy event descriptors:
     Invalid word count: 0
     Running disparity error count: 0
     Loss of dword synchronization count: 0
     Phy reset problem count: 0
     Elasticity buffer overflow count: 0
     Received abandon-class OPEN_REJECT count: 0
     Transmitted BREAK count: 0
     Received BREAK count: 0
     Transmitted SSP frame error count: 0
     Received SSP frame error count: 0

