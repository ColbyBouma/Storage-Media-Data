[CmdletBinding()]
param (
    [Switch]$Raw
)

Function Get-PowerOnTime {

    Get-ChildItem -Path '..\' -Recurse -Include 'SMART.json' -Exclude 'Scripts' | ForEach-Object {
        
        $SmartJson = Get-Content -Path $_.FullName -Raw | ConvertFrom-Json

        $Attribute9 = $SmartJson.ata_smart_attributes.table | Where-Object id -eq 9

        $Protocol, $Type, $Brand, $Family, $Model, $Serial = ($_ -split '\\')[-7..-2]
        
        [PSCustomObject]@{
            'Protocol'       = $Protocol
            'Type'           = $Type
            'Brand'          = $Brand
            'Family'         = $Family
            'Model'          = $Model
            'Serial'         = $Serial
            'Attribute Name' = $Attribute9.name
            'Computed Hours' = $SmartJson.power_on_time.hours
            'Raw String'     = $Attribute9.raw.string
            'Raw Value'      = $Attribute9.raw.value
        }

    }

}

if ( $Raw ) {

    Get-PowerOnTime

} else {

    Get-PowerOnTime | Format-Table -AutoSize

}