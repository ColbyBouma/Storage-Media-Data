[CmdletBinding()]
param (
    [String]$CsvPath
)

if ( $CsvPath ) {

    Remove-Item -Force -Path $CsvPath -ErrorAction 'SilentlyContinue'

}

$Drives = New-Object System.Collections.Generic.List[object]
$IdList = @{}

Get-ChildItem -Path '..\' -Recurse -Include 'SMART.json' -Exclude 'SAS', 'Scripts', 'NVMe' | ForEach-Object {

    $SmartJson = Get-Content $_.FullName | ConvertFrom-Json

    $Protocol, $Type, $Brand, $Family, $Model, $Serial = ($_ -split '\\')[-7..-2]
    
    $Drive = @{
        'Protocol' = $Protocol
        'Type'     = $Type
        'Brand'    = $Brand
        'Family'   = $Family
        'Model'    = $Model
        'Serial'   = $Serial
        'Bytes'    = $SmartJson.user_capacity.bytes
        'In DB'    = $SmartJson.in_smartctl_database
    }
    
    Foreach ( $Attribute in $SmartJson.ata_smart_attributes.table ) {

        # Add the attribute to the $Drive hashtable.
        # Cast the id to [String] so the existing values don't get overwritten.
        $Drive.([String]$Attribute.id) = $Attribute.name

        # Add the attribute's ID to the list of all IDs. No need to check if it already exists.
        # Cast to [Int32] so the keys will sort properly below.
        $IdList.([Int32]$Attribute.id) = $null

    }

    $Drives.Add($Drive)

}

$IdListSorted = [Int32[]]$IdList.Keys | Sort-Object

Foreach ( $Drive in $Drives ) {

    # $Output has to be created every time because you can't clone ordered dictionaries.
    $Output = [Ordered]@{
        'Protocol' = $null
        'Type'     = $null
        'Brand'    = $null
        'Family'   = $null
        'Model'    = $null
        'Serial'   = $null
        'Bytes'    = $null
        'In DB'    = $null
    }
    # In order for the final output to have all IDs, you have to add them all every time.
    Foreach ( $Id in $IdListSorted ) {

        $Output.[String]$Id = $null

    }

    Foreach ( $Attribute in $Drive.GetEnumerator() ) {

        # Fill the drive's attributes into the template.
        $Output.($Attribute.Key) = $Attribute.Value

    }

    if ( $CsvPath ) {

        [PSCustomObject]$Output | Export-Csv -Path $CsvPath -NoTypeInformation -Append

    } else {

        [PSCustomObject]$Output

    }

}