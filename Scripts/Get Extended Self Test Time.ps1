$ModelNameTable = @{}

Get-ChildItem -Path $PSScriptRoot\.. -Recurse -Include 'SMART.json' | ForEach-Object {

    $SmartJson = Get-Content -Path $_.FullName -Raw | ConvertFrom-Json

    $Protocol, $Type, $Brand, $Family, $Model, $Serial = ($_ -split '\\')[-7..-2]

    $ModelName = $Protocol, $Type, $Brand, $Family, $Model -join '-'
    if ( (-not $ModelNameTable[$ModelName]) -and ($Protocol -notin 'NVMe', 'SAS') ) {

        $ModelNameTable.Add($ModelName, $true)

        $Size = switch ([Math]::Ceiling((("$($SmartJson.user_capacity.bytes)").Length / 3))) {
            1 { '{0} bytes' -f $SmartJson.user_capacity.bytes; break }
            2 { '{0} kB' -f [Math]::Round(($SmartJson.user_capacity.bytes / 1000), 1); break }
            3 { '{0} MB' -f [Math]::Round(($SmartJson.user_capacity.bytes / 1000000), 1); break }
            4 { '{0} GB' -f [Math]::Round(($SmartJson.user_capacity.bytes / 1000000000), 1); break }
            5 { '{0} TB' -f [Math]::Round(($SmartJson.user_capacity.bytes / 1000000000000), 1); break }
        }
    
        [PSCustomObject]@{
            'Protocol'  = $Protocol
            'Type'      = $Type
            'Brand'     = $Brand
            'Model'     = $Model
            'Family'    = $Family
            'Size'      = $Size
            'Test Time' = $SmartJson.ata_smart_data.self_test.polling_minutes.extended
        }

    }

}