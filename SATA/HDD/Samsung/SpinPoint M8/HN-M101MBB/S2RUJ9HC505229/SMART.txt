smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.11.0-25-generic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Seagate Samsung SpinPoint M8 (AF)
Device Model:     ST1000LM024 HN-M101MBB
Serial Number:    S2RUJ9HC505229
LU WWN Device Id: 5 0004cf 2076b3693
Firmware Version: 2AR10001
User Capacity:    1,000,204,886,016 bytes [1.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5400 rpm
Form Factor:      2.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 3.0 Gb/s (current: 3.0 Gb/s)
Local Time is:    Sat Aug 21 18:57:50 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM level is:     128 (quiet), recommended: 254
APM feature is:   Disabled
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Read SCT Status failed: Input/output error
Wt Cache Reorder: Unknown (SCT Feature Control command failed)

Read SMART Data failed: Input/output error

=== START OF READ SMART DATA SECTION ===
SMART Status command failed: Input/output error
SMART overall-health self-assessment test result: UNKNOWN!
SMART Status, Attributes and Thresholds cannot be read.

Read SMART Log Directory failed: Input/output error

ATA_READ_LOG_EXT (addr=0x00:0x00, page=0, n=1) failed: Input/output error
Read GP Log Directory failed

SMART Extended Comprehensive Error Log (GP Log 0x03) not supported

Read SMART Error Log failed: Input/output error

SMART Extended Self-test Log (GP Log 0x07) not supported

Read SMART Self-test Log failed: Input/output error

Selective Self-tests/Logging not supported

Read SCT Status failed: Input/output error

Read SCT Status failed: Input/output error
SCT (Get) Error Recovery Control command failed

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

ATA_READ_LOG_EXT (addr=0x11:0x00, page=0, n=1) failed: Input/output error
Read SATA Phy Event Counters failed

