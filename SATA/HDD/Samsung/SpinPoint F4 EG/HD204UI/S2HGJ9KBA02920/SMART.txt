smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.11.0-36-generic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     SAMSUNG SpinPoint F4 EG (AF)
Device Model:     SAMSUNG HD204UI
Serial Number:    S2HGJ9KBA02920
LU WWN Device Id: 5 0024e9 206686c2f
Firmware Version: 1AQ10001
User Capacity:    2,000,398,934,016 bytes [2.00 TB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    5400 rpm
Form Factor:      3.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 2.6, 3.0 Gb/s
Local Time is:    Wed Sep 22 15:10:05 2021 MDT

==> WARNING: Using smartmontools or hdparm with this
drive may result in data loss due to a firmware bug.
****** THIS DRIVE MAY OR MAY NOT BE AFFECTED! ******
Buggy and fixed firmware report same version number!
See the following web pages for details:
http://knowledge.seagate.com/articles/en_US/FAQ/223571en
https://www.smartmontools.org/wiki/SamsungF4EGBadBlocks

SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Disabled
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(20160) seconds.
Offline data collection
capabilities: 			 (0x5b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 336) minutes.
SCT capabilities: 	       (0x003f)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   100   100   051    -    649
  2 Throughput_Performance  -OS--K   252   252   000    -    0
  3 Spin_Up_Time            PO---K   067   065   025    -    10170
  4 Start_Stop_Count        -O--CK   095   095   000    -    5195
  5 Reallocated_Sector_Ct   PO--CK   252   252   010    -    0
  7 Seek_Error_Rate         -OSR-K   252   252   051    -    0
  8 Seek_Time_Performance   --S--K   252   252   015    -    0
  9 Power_On_Hours          -O--CK   100   100   000    -    43927
 10 Spin_Retry_Count        -O--CK   252   252   051    -    0
 11 Calibration_Retry_Count -O--CK   252   252   000    -    0
 12 Power_Cycle_Count       -O--CK   099   099   000    -    1045
181 Program_Fail_Cnt_Total  -O---K   093   093   000    -    154976030
191 G-Sense_Error_Rate      -O---K   100   100   000    -    9
192 Power-Off_Retract_Count -O---K   252   252   000    -    0
194 Temperature_Celsius     -O----   064   064   000    -    22 (Min/Max 5/34)
195 Hardware_ECC_Recovered  -O-RCK   100   100   000    -    0
196 Reallocated_Event_Count -O--CK   252   252   000    -    0
197 Current_Pending_Sector  -O--CK   252   252   000    -    0
198 Offline_Uncorrectable   ----CK   252   252   000    -    0
199 UDMA_CRC_Error_Count    -OS-CK   200   200   000    -    0
200 Multi_Zone_Error_Rate   -O-R-K   100   100   000    -    792
223 Load_Retry_Count        -O--CK   252   252   000    -    0
225 Load_Cycle_Count        -O--CK   100   100   000    -    5206
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      2  Comprehensive SMART error log
0x03       GPL     R/O      2  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      2  Extended self-test log
0x08       GPL     R/O      2  Power Conditions log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (2 sectors)
No Errors Logged

SMART Extended Self-test Log Version: 1 (2 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 0
Note: revision number not 1 implies that no selective self-test has ever been run
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Completed [00% left] (0-65535)
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  2
SCT Version (vendor specific):       256 (0x0100)
Device State:                        Active (0)
Current Temperature:                    22 Celsius
Power Cycle Min/Max Temperature:     20/22 Celsius
Lifetime    Min/Max Temperature:      8/62 Celsius
Specified Max Operating Temperature:    80 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         5 minutes
Temperature Logging Interval:        5 minutes
Min/Max recommended Temperature:     -5/80 Celsius
Min/Max Temperature Limit:           -10/85 Celsius
Temperature History Size (Index):    128 (109)

Index    Estimated Time   Temperature Celsius
 110    2021-09-22 04:35    22  ***
 111    2021-09-22 04:40    23  ****
 112    2021-09-22 04:45    23  ****
 113    2021-09-22 04:50    23  ****
 114    2021-09-22 04:55    19  -
 115    2021-09-22 05:00    18  -
 116    2021-09-22 05:05    19  -
 117    2021-09-22 05:10    20  *
 118    2021-09-22 05:15    21  **
 119    2021-09-22 05:20    21  **
 120    2021-09-22 05:25    18  -
 121    2021-09-22 05:30    19  -
 122    2021-09-22 05:35    20  *
 123    2021-09-22 05:40    21  **
 124    2021-09-22 05:45    21  **
 125    2021-09-22 05:50    19  -
 126    2021-09-22 05:55    20  *
 127    2021-09-22 06:00    21  **
   0    2021-09-22 06:05    22  ***
   1    2021-09-22 06:10    22  ***
   2    2021-09-22 06:15    23  ****
   3    2021-09-22 06:20    23  ****
   4    2021-09-22 06:25    23  ****
   5    2021-09-22 06:30    24  *****
   6    2021-09-22 06:35    24  *****
   7    2021-09-22 06:40    26  *******
   8    2021-09-22 06:45    24  *****
 ...    ..(  2 skipped).    ..  *****
  11    2021-09-22 07:00    24  *****
  12    2021-09-22 07:05    22  ***
  13    2021-09-22 07:10    22  ***
  14    2021-09-22 07:15    23  ****
  15    2021-09-22 07:20    23  ****
  16    2021-09-22 07:25    23  ****
  17    2021-09-22 07:30    21  **
  18    2021-09-22 07:35    21  **
  19    2021-09-22 07:40    22  ***
  20    2021-09-22 07:45    23  ****
 ...    ..(  3 skipped).    ..  ****
  24    2021-09-22 08:05    23  ****
  25    2021-09-22 08:10    20  *
  26    2021-09-22 08:15    21  **
  27    2021-09-22 08:20    22  ***
  28    2021-09-22 08:25    23  ****
  29    2021-09-22 08:30    23  ****
  30    2021-09-22 08:35    20  *
  31    2021-09-22 08:40    21  **
  32    2021-09-22 08:45    22  ***
  33    2021-09-22 08:50    22  ***
  34    2021-09-22 08:55    23  ****
 ...    ..(  2 skipped).    ..  ****
  37    2021-09-22 09:10    23  ****
  38    2021-09-22 09:15    24  *****
 ...    ..(  2 skipped).    ..  *****
  41    2021-09-22 09:30    24  *****
  42    2021-09-22 09:35    22  ***
  43    2021-09-22 09:40    23  ****
  44    2021-09-22 09:45    23  ****
  45    2021-09-22 09:50    23  ****
  46    2021-09-22 09:55    20  *
  47    2021-09-22 10:00    21  **
  48    2021-09-22 10:05    22  ***
  49    2021-09-22 10:10    24  *****
  50    2021-09-22 10:15    23  ****
  51    2021-09-22 10:20    21  **
  52    2021-09-22 10:25    22  ***
  53    2021-09-22 10:30    23  ****
  54    2021-09-22 10:35    23  ****
  55    2021-09-22 10:40    23  ****
  56    2021-09-22 10:45    24  *****
  57    2021-09-22 10:50    24  *****
  58    2021-09-22 10:55    20  *
  59    2021-09-22 11:00    21  **
  60    2021-09-22 11:05    22  ***
  61    2021-09-22 11:10    23  ****
  62    2021-09-22 11:15    24  *****
  63    2021-09-22 11:20    24  *****
  64    2021-09-22 11:25    20  *
  65    2021-09-22 11:30    22  ***
  66    2021-09-22 11:35    23  ****
  67    2021-09-22 11:40    23  ****
  68    2021-09-22 11:45    21  **
  69    2021-09-22 11:50    22  ***
  70    2021-09-22 11:55    23  ****
  71    2021-09-22 12:00    23  ****
  72    2021-09-22 12:05    24  *****
  73    2021-09-22 12:10    24  *****
  74    2021-09-22 12:15    19  -
  75    2021-09-22 12:20    20  *
  76    2021-09-22 12:25    21  **
  77    2021-09-22 12:30    22  ***
  78    2021-09-22 12:35    22  ***
  79    2021-09-22 12:40    19  -
  80    2021-09-22 12:45    21  **
  81    2021-09-22 12:50    22  ***
  82    2021-09-22 12:55    22  ***
  83    2021-09-22 13:00    23  ****
  84    2021-09-22 13:05    19  -
  85    2021-09-22 13:10    20  *
  86    2021-09-22 13:15    21  **
  87    2021-09-22 13:20    22  ***
  88    2021-09-22 13:25    23  ****
  89    2021-09-22 13:30    19  -
  90    2021-09-22 13:35    20  *
  91    2021-09-22 13:40    22  ***
  92    2021-09-22 13:45    22  ***
  93    2021-09-22 13:50    23  ****
  94    2021-09-22 13:55    19  -
  95    2021-09-22 14:00    21  **
  96    2021-09-22 14:05    21  **
  97    2021-09-22 14:10    22  ***
  98    2021-09-22 14:15    23  ****
  99    2021-09-22 14:20    16  -
 100    2021-09-22 14:25    18  -
 101    2021-09-22 14:30    19  -
 102    2021-09-22 14:35    20  *
 103    2021-09-22 14:40    20  *
 104    2021-09-22 14:45    16  -
 105    2021-09-22 14:50    17  -
 106    2021-09-22 14:55    18  -
 107    2021-09-22 15:00    19  -
 108    2021-09-22 15:05    20  *
 109    2021-09-22 15:10    21  **

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  4            0  Command failed due to ICRC error
0x0002  4            0  R_ERR response for data FIS
0x0003  4            0  R_ERR response for device-to-host data FIS
0x0004  4            0  R_ERR response for host-to-device data FIS
0x0005  4            0  R_ERR response for non-data FIS
0x0006  4            0  R_ERR response for device-to-host non-data FIS
0x0007  4            0  R_ERR response for host-to-device non-data FIS
0x0008  4            0  Device-to-host non-data FIS retries
0x0009  4            1  Transition from drive PhyRdy to drive PhyNRdy
0x000a  4            0  Device-to-host register FISes sent due to a COMRESET
0x000b  4            0  CRC errors within host-to-device FIS
0x000d  4            0  Non-CRC errors within host-to-device FIS
0x000f  4            0  R_ERR response for host-to-device data FIS, CRC
0x0010  4            0  R_ERR response for host-to-device data FIS, non-CRC
0x0012  4            0  R_ERR response for host-to-device non-data FIS, CRC
0x0013  4            0  R_ERR response for host-to-device non-data FIS, non-CRC
0x8e00  4            0  Vendor specific
0x8e01  4            0  Vendor specific
0x8e02  4            0  Vendor specific
0x8e03  4            0  Vendor specific
0x8e04  4            0  Vendor specific
0x8e05  4            0  Vendor specific
0x8e06  4            0  Vendor specific
0x8e07  4            0  Vendor specific
0x8e08  4            0  Vendor specific
0x8e09  4            0  Vendor specific
0x8e0a  4            0  Vendor specific
0x8e0b  4            0  Vendor specific
0x8e0c  4            0  Vendor specific
0x8e0d  4            0  Vendor specific
0x8e0e  4            0  Vendor specific
0x8e0f  4            0  Vendor specific
0x8e10  4            0  Vendor specific
0x8e11  4            0  Vendor specific

