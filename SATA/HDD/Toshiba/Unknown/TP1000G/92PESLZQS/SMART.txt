smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.11.0-25-generic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     Teleplan TP1000G
Serial Number:    92PESLZQS
LU WWN Device Id: 0 000000 000000000
Firmware Version: AX001U
User Capacity:    1,000,204,886,016 bytes [1.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5400 rpm
Form Factor:      2.5 inches
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   ATA8-ACS (minor revision not indicated)
SATA Version is:  SATA 2.6, 3.0 Gb/s (current: 3.0 Gb/s)
Local Time is:    Sat Aug 21 22:27:12 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: FAILED!
Drive failure expected in less than 24 hours. SAVE ALL DATA.
See vendor-specific Attribute list for failed Attributes.

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(  120) seconds.
Offline data collection
capabilities: 			 (0x5b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 246) minutes.
SCT capabilities: 	       (0x003d)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     PO-R--   099   099   050    -    0
  2 Throughput_Performance  P-S---   100   100   050    -    0
  3 Spin_Up_Time            POS--K   100   100   001    -    2630
  4 Start_Stop_Count        -O--CK   100   100   000    -    27
  5 Reallocated_Sector_Ct   PO--CK   001   001   050    NOW  16376
  7 Seek_Error_Rate         PO-R--   100   100   050    -    0
  8 Seek_Time_Performance   P-S---   100   100   050    -    0
  9 Power_On_Hours          -O--CK   100   100   000    -    10
 10 Spin_Retry_Count        PO--CK   100   100   030    -    0
 12 Power_Cycle_Count       -O--CK   100   100   000    -    26
191 G-Sense_Error_Rate      -O--CK   100   100   000    -    0
192 Power-Off_Retract_Count -O--CK   100   100   000    -    18
193 Load_Cycle_Count        -O--CK   100   100   000    -    36
194 Temperature_Celsius     -O---K   100   100   000    -    24 (Min/Max 18/38)
196 Reallocated_Event_Count -O--CK   100   100   000    -    897
197 Current_Pending_Sector  -O--CK   100   100   000    -    0
198 Offline_Uncorrectable   ----CK   100   100   000    -    0
199 UDMA_CRC_Error_Count    -O--CK   200   253   000    -    0
220 Disk_Shift              -O----   100   100   000    -    0
222 Loaded_Hours            -O--CK   100   100   000    -    9
223 Load_Retry_Count        -O--CK   100   100   000    -    0
224 Load_Friction           -O---K   100   100   000    -    0
226 Load-in_Time            -OS--K   100   100   000    -    46248
240 Head_Flying_Hours       P-----   100   100   001    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O     51  Comprehensive SMART error log
0x03       GPL     R/O     64  Ext. Comprehensive SMART error log
0x04       GPL,SL  R/O      8  Device Statistics log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (64 sectors)
Device Error Count: 6
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 6 [5] occurred at disk power-on lifetime: 10 hours (0 days + 10 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 17 00 00 00 0a 4d 10 40 00  Error: UNC 23 sectors at LBA = 0x000a4d10 = 675088

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 7f 00 00 00 0a 4c a8 40 00     00:00:54.081  READ DMA EXT
  25 00 00 00 7f 00 00 00 0a 4c 29 40 00     00:00:54.079  READ DMA EXT
  25 00 00 00 7f 00 00 00 0a 4b aa 40 00     00:00:54.077  READ DMA EXT
  25 00 00 00 3f 00 00 00 0a 4b 6b 40 00     00:00:54.063  READ DMA EXT
  25 00 00 00 7f 00 00 00 0a 4a ec 40 00     00:00:54.061  READ DMA EXT

Error 5 [4] occurred at disk power-on lifetime: 0 hours (0 days + 0 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 31 00 01 00 00 00 0a 4d 10 e0 00  Device Fault; Error: ABRT 1 sectors at LBA = 0x000a4d10 = 675088

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 01 00 00 00 0a 4d 11 e0 00     00:02:34.592  READ DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 10 e0 00     00:02:34.589  READ DMA EXT
  35 00 00 00 01 00 00 00 0a 4d 10 e0 00     00:02:34.589  WRITE DMA EXT
  35 00 00 00 01 00 00 00 0a 4d 0f e0 00     00:02:34.589  WRITE DMA EXT
  35 00 00 00 01 00 00 00 0a 4d 10 e0 00     00:02:34.588  WRITE DMA EXT

Error 4 [3] occurred at disk power-on lifetime: 0 hours (0 days + 0 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 01 00 00 00 0a 4d 10 e0 00  Error: UNC 1 sectors at LBA = 0x000a4d10 = 675088

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 01 00 00 00 0a 4d 10 e0 00     00:02:29.011  READ DMA EXT
  35 00 00 00 01 00 00 00 0a 4d 0f e0 00     00:02:29.000  WRITE DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 0f e0 00     00:02:28.957  READ DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 10 e0 00     00:02:26.146  READ DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 0f e0 00     00:02:26.144  READ DMA EXT

Error 3 [2] occurred at disk power-on lifetime: 0 hours (0 days + 0 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 01 00 00 00 0a 4d 10 e0 00  Error: UNC 1 sectors at LBA = 0x000a4d10 = 675088

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 01 00 00 00 0a 4d 10 e0 00     00:02:26.146  READ DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 0f e0 00     00:02:26.144  READ DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 0e e0 00     00:02:26.142  READ DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 0d e0 00     00:02:26.140  READ DMA EXT
  25 00 00 00 01 00 00 00 0a 4d 0c e0 00     00:02:26.138  READ DMA EXT

Error 2 [1] occurred at disk power-on lifetime: 0 hours (0 days + 0 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 30 00 00 00 0a 4d 10 e0 00  Error: UNC 48 sectors at LBA = 0x000a4d10 = 675088

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 80 00 00 00 0a 4c c0 e0 00     00:02:21.457  READ DMA EXT
  25 00 00 00 80 00 00 00 0a 4c 40 e0 00     00:02:21.455  READ DMA EXT
  25 00 00 00 80 00 00 00 0a 4b c0 e0 00     00:02:21.453  READ DMA EXT
  25 00 00 00 80 00 00 00 0a 4b 40 e0 00     00:02:21.451  READ DMA EXT
  25 00 00 00 78 00 00 00 0a 4a c8 e0 00     00:02:21.446  READ DMA EXT

Error 1 [0] occurred at disk power-on lifetime: 0 hours (0 days + 0 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 38 00 00 00 5f 5d d0 e0 00  Error: UNC 56 sectors at LBA = 0x005f5dd0 = 6249936

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 80 00 00 00 5f 5d 88 e0 00     00:15:37.233  READ DMA EXT
  25 00 00 00 80 00 00 00 5f 5d 08 e0 00     00:15:36.655  READ DMA EXT
  25 00 00 00 80 00 00 00 5f 5c 88 e0 00     00:15:36.484  READ DMA EXT
  25 00 00 00 80 00 00 00 5f 5c 08 e0 00     00:15:36.418  READ DMA EXT
  25 00 00 00 80 00 00 00 5f 5b 88 e0 00     00:15:36.352  READ DMA EXT

SMART Extended Self-test Log Version: 1 (1 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  2
SCT Version (vendor specific):       1 (0x0001)
Device State:                        Active (0)
Current Temperature:                    24 Celsius
Power Cycle Min/Max Temperature:     23/24 Celsius
Lifetime    Min/Max Temperature:     18/38 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      5/55 Celsius
Min/Max Temperature Limit:            5/55 Celsius
Temperature History Size (Index):    478 (28)

Index    Estimated Time   Temperature Celsius
  29    2021-08-21 14:30     ?  -
 ...    ..(448 skipped).    ..  -
   0    2021-08-21 21:59     ?  -
   1    2021-08-21 22:00    28  *********
   2    2021-08-21 22:01     ?  -
   3    2021-08-21 22:02     ?  -
   4    2021-08-21 22:03    30  ***********
   5    2021-08-21 22:04    31  ************
   6    2021-08-21 22:05     ?  -
   7    2021-08-21 22:06    26  *******
   8    2021-08-21 22:07     ?  -
   9    2021-08-21 22:08     ?  -
  10    2021-08-21 22:09    20  *
  11    2021-08-21 22:10     ?  -
  12    2021-08-21 22:11    29  **********
  13    2021-08-21 22:12    30  ***********
  14    2021-08-21 22:13    30  ***********
  15    2021-08-21 22:14    30  ***********
  16    2021-08-21 22:15    31  ************
  17    2021-08-21 22:16     ?  -
  18    2021-08-21 22:17    29  **********
  19    2021-08-21 22:18    29  **********
  20    2021-08-21 22:19    30  ***********
  21    2021-08-21 22:20     ?  -
  22    2021-08-21 22:21    21  **
  23    2021-08-21 22:22     ?  -
  24    2021-08-21 22:23    38  *******************
  25    2021-08-21 22:24    36  *****************
  26    2021-08-21 22:25    36  *****************
  27    2021-08-21 22:26     ?  -
  28    2021-08-21 22:27    24  *****

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP Log 0x04)
Page  Offset Size        Value Flags Description
0x01  =====  =               =  ===  == General Statistics (rev 2) ==
0x01  0x008  4              26  ---  Lifetime Power-On Resets
0x01  0x010  4              10  ---  Power-on Hours
0x01  0x018  6      1953764853  ---  Logical Sectors Written
0x01  0x020  6        15264709  ---  Number of Write Commands
0x01  0x028  6         2584683  ---  Logical Sectors Read
0x01  0x030  6           44498  ---  Number of Read Commands
0x02  =====  =               =  ===  == Free-Fall Statistics (rev 1) ==
0x02  0x010  4               0  ---  Overlimit Shock Events
0x03  =====  =               =  ===  == Rotating Media Statistics (rev 1) ==
0x03  0x008  4              10  ---  Spindle Motor Power-on Hours
0x03  0x010  4               9  ---  Head Flying Hours
0x03  0x018  4              36  ---  Head Load Events
0x03  0x020  4           16376  ---  Number of Reallocated Logical Sectors
0x03  0x028  4               7  ---  Read Recovery Attempts
0x03  0x030  4               0  ---  Number of Mechanical Start Failures
0x04  =====  =               =  ===  == General Errors Statistics (rev 1) ==
0x04  0x008  4               5  ---  Number of Reported Uncorrectable Errors
0x04  0x010  4               1  ---  Resets Between Cmd Acceptance and Completion
0x05  =====  =               =  ===  == Temperature Statistics (rev 1) ==
0x05  0x008  1              24  ---  Current Temperature
0x05  0x010  1               -  N--  Average Short Term Temperature
0x05  0x018  1               -  N--  Average Long Term Temperature
0x05  0x020  1              38  ---  Highest Temperature
0x05  0x028  1              18  ---  Lowest Temperature
0x05  0x030  1               -  N--  Highest Average Short Term Temperature
0x05  0x038  1               -  N--  Lowest Average Short Term Temperature
0x05  0x040  1               -  N--  Highest Average Long Term Temperature
0x05  0x048  1               -  N--  Lowest Average Long Term Temperature
0x05  0x050  4               0  ---  Time in Over-Temperature
0x05  0x058  1              55  ---  Specified Maximum Operating Temperature
0x05  0x060  4               0  ---  Time in Under-Temperature
0x05  0x068  1               5  ---  Specified Minimum Operating Temperature
0x06  =====  =               =  ===  == Transport Statistics (rev 1) ==
0x06  0x008  4               3  ---  Number of Hardware Resets
0x06  0x018  4               0  ---  Number of Interface CRC Errors
0x07  =====  =               =  ===  == Solid State Device Statistics (rev 1) ==
0x07  0x008  1               0  N--  Percentage Used Endurance Indicator
                                |||_ C monitored condition met
                                ||__ D supports DSN
                                |___ N normalized value

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  4            0  Command failed due to ICRC error
0x0002  4            0  R_ERR response for data FIS
0x0003  4            0  R_ERR response for device-to-host data FIS
0x0004  4            0  R_ERR response for host-to-device data FIS
0x0005  4            0  R_ERR response for non-data FIS
0x0006  4            0  R_ERR response for device-to-host non-data FIS
0x0007  4            0  R_ERR response for host-to-device non-data FIS
0x0008  4            0  Device-to-host non-data FIS retries
0x0009  4            1  Transition from drive PhyRdy to drive PhyNRdy
0x000a  4            0  Device-to-host register FISes sent due to a COMRESET
0x000b  4            0  CRC errors within host-to-device FIS
0x000d  4            0  Non-CRC errors within host-to-device FIS
0x000f  4            0  R_ERR response for host-to-device data FIS, CRC
0x0010  4            0  R_ERR response for host-to-device data FIS, non-CRC
0x0012  4            0  R_ERR response for host-to-device non-data FIS, CRC
0x0013  4            0  R_ERR response for host-to-device non-data FIS, non-CRC

