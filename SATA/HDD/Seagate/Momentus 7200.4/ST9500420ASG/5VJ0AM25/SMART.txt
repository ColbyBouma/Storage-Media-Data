smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.11.0-25-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Seagate Momentus 7200.4
Device Model:     ST9500420ASG
Serial Number:    5VJ0AM25
LU WWN Device Id: 5 000c50 016804720
Firmware Version: 0002SDM1
User Capacity:    500,107,862,016 bytes [500 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    7200 rpm
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS T13/1699-D revision 4
SATA Version is:  SATA 2.6, 3.0 Gb/s
Local Time is:    Sat Aug 14 22:39:30 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM level is:     208 (intermediate), recommended: 208
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Unknown

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: FAILED!
Drive failure expected in less than 24 hours. SAVE ALL DATA.
See vendor-specific Attribute list for failed Attributes.

General SMART Values:
Offline data collection status:  (0x82)	Offline data collection activity
					was completed without error.
					Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    0) seconds.
Offline data collection
capabilities: 			 (0x7b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 109) minutes.
Conveyance self-test routine
recommended polling time: 	 (   3) minutes.
SCT capabilities: 	       (0x103f)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR--   088   081   006    -    169145377
  3 Spin_Up_Time            PO----   099   098   085    -    0
  4 Start_Stop_Count        -O--CK   099   099   020    -    1590
  5 Reallocated_Sector_Ct   PO--CK   001   001   036    NOW  2052
  7 Seek_Error_Rate         POSR--   074   060   030    -    34568699029
  9 Power_On_Hours          -O--CK   079   079   000    -    19062
 10 Spin_Retry_Count        PO--C-   100   100   097    -    0
 12 Power_Cycle_Count       -O--CK   099   037   020    -    1483
184 End-to-End_Error        -O--CK   100   100   099    -    0
187 Reported_Uncorrect      -O--CK   001   001   000    -    3264
188 Command_Timeout         -O--CK   100   081   000    -    124555952158
189 High_Fly_Writes         -O-RCK   100   100   000    -    0
190 Airflow_Temperature_Cel -O---K   075   053   045    -    25 (Min/Max 25/25)
191 G-Sense_Error_Rate      -O--CK   100   100   000    -    0
192 Power-Off_Retract_Count -O--CK   100   100   000    -    276
193 Load_Cycle_Count        -O--CK   001   001   000    -    2077224
194 Temperature_Celsius     -O---K   025   047   000    -    25 (243 52 0 0 0)
195 Hardware_ECC_Recovered  -O-RC-   045   040   000    -    169145377
197 Current_Pending_Sector  -O--C-   100   100   000    -    2
198 Offline_Uncorrectable   ----C-   100   100   000    -    2
199 UDMA_CRC_Error_Count    -OSRCK   200   200   000    -    0
240 Head_Flying_Hours       ------   100   253   000    -    10527 (180 216 0)
241 Total_LBAs_Written      ------   100   253   000    -    729250571
242 Total_LBAs_Read         ------   100   253   000    -    1859109471
254 Free_Fall_Sensor        -O--CK   001   001   000    -    120
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01       GPL,SL  R/O      1  Summary SMART error log
0x02       GPL,SL  R/O      5  Comprehensive SMART error log
0x03       GPL,SL  R/O      5  Ext. Comprehensive SMART error log
0x06       GPL,SL  R/O      1  SMART self-test log
0x07       GPL,SL  R/O      1  Extended self-test log
0x09       GPL,SL  R/W      1  Selective self-test log
0x10       GPL,SL  R/O      1  NCQ Command Error log
0x11       GPL,SL  R/O      1  SATA Phy Event Counters log
0x21       GPL,SL  R/O      1  Write stream error log
0x22       GPL,SL  R/O      1  Read stream error log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xa1       GPL,SL  VS      20  Device vendor specific log
0xa2       GPL     VS    2248  Device vendor specific log
0xa8       GPL,SL  VS      65  Device vendor specific log
0xa9       GPL,SL  VS       1  Device vendor specific log
0xb0       GPL     VS    2864  Device vendor specific log
0xbe-0xbf  GPL     VS   65535  Device vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (5 sectors)
Device Error Count: 14905 (device log contains only the most recent 20 errors)
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 14905 [4] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 9d e0 00  Device Fault; Error: ABRT 4 sectors at LBA = 0x0000329d = 12957

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 01 00 00 40 00 06 2a e0 00     00:14:18.885  READ DMA EXT
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:14:18.832  NOP [Abort queued commands]
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:13:48.037  NOP [Abort queued commands]
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:13:17.243  NOP [Abort queued commands]
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:12:41.906  NOP [Abort queued commands]

Error 14904 [3] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 9d e0 00  Device Fault; Error: ABRT 4 sectors at LBA = 0x0000329d = 12957

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 10 00 00 bb 00 4d d8 e0 00     00:00:09.987  READ DMA EXT
  25 00 00 00 10 00 00 fb 00 4a 18 e0 00     00:00:09.977  READ DMA EXT
  25 00 00 00 08 00 00 ec 00 4a 98 e0 00     00:00:09.958  READ DMA EXT
  25 00 00 00 08 00 00 f4 00 07 98 e0 00     00:00:09.944  READ DMA EXT
  25 00 00 00 01 00 00 40 00 06 2a e0 00     00:00:09.916  READ DMA EXT

Error 14903 [2] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 9d e0 00  Device Fault; Error: ABRT 4 sectors at LBA = 0x0000329d = 12957

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 01 00 00 40 00 06 2a e0 00     00:14:32.340  READ DMA EXT
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:14:32.257  NOP [Abort queued commands]
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:14:01.462  NOP [Abort queued commands]
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:13:30.666  NOP [Abort queued commands]
  00 00 00 00 00 00 00 00 00 00 00 00 ff     00:12:55.340  NOP [Abort queued commands]

Error 14902 [1] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 9d e0 00  Device Fault; Error: ABRT 4 sectors at LBA = 0x0000329d = 12957

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 10 00 00 bb 00 4d d8 e0 00     00:00:11.750  READ DMA EXT
  25 00 00 00 10 00 00 fb 00 4a 18 e0 00     00:00:11.739  READ DMA EXT
  25 00 00 00 08 00 00 ec 00 4a 98 e0 00     00:00:11.729  READ DMA EXT
  25 00 00 00 08 00 00 f4 00 07 98 e0 00     00:00:11.715  READ DMA EXT
  25 00 00 00 01 00 00 40 00 06 2a e0 00     00:00:11.686  READ DMA EXT

Error 14901 [0] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 9d e0 00  Device Fault; Error: ABRT 4 sectors at LBA = 0x0000329d = 12957

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 00 10 00 00 b9 00 4d b8 e0 00     00:00:38.984  READ DMA EXT
  25 00 00 00 10 00 00 f8 00 4c f8 e0 00     00:00:38.974  READ DMA EXT
  ea 00 00 00 00 00 00 00 00 00 00 e0 00     00:00:38.971  FLUSH CACHE EXT
  ea 00 00 00 00 00 00 00 00 00 00 e0 00     00:00:38.970  FLUSH CACHE EXT
  ea 00 00 00 00 00 00 00 00 00 00 e0 00     00:00:38.969  FLUSH CACHE EXT

Error 14900 [19] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 91 40 00  Device Fault; Error: ABRT

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  ef 00 aa 00 00 00 00 00 00 00 00 00 00  4d+17:46:59.418  SET FEATURES [Enable read look-ahead]
  ef 00 03 00 46 00 00 00 00 00 00 00 00  4d+17:46:59.417  SET FEATURES [Set transfer mode]
  00 00 00 00 00 00 00 00 00 00 00 00 ff  4d+17:46:57.405  NOP [Abort queued commands]
  2f 00 00 00 01 00 00 00 00 00 10 00 00  4d+17:46:56.897  READ LOG EXT
  60 00 00 00 08 00 02 3e 00 6a 98 40 00  4d+17:46:56.897  READ FPDMA QUEUED

Error 14899 [18] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 91 40 00  Device Fault; Error: ABRT

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  ef 00 03 00 46 00 00 00 00 00 00 00 00  4d+17:46:59.417  SET FEATURES [Set transfer mode]
  00 00 00 00 00 00 00 00 00 00 00 00 ff  4d+17:46:57.405  NOP [Abort queued commands]
  2f 00 00 00 01 00 00 00 00 00 10 00 00  4d+17:46:56.897  READ LOG EXT
  60 00 00 00 08 00 02 3e 00 6a 98 40 00  4d+17:46:56.897  READ FPDMA QUEUED
  25 00 00 00 08 00 02 3e 00 6a 98 40 00  4d+17:46:55.937  READ DMA EXT

Error 14898 [17] occurred at disk power-on lifetime: 19057 hours (794 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  04 -- 71 00 04 00 00 00 00 32 91 40 00  Device Fault; Error: ABRT

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  2f 00 00 00 01 00 00 00 00 00 10 00 00  4d+17:46:56.897  READ LOG EXT
  60 00 00 00 08 00 02 3e 00 6a 98 40 00  4d+17:46:56.897  READ FPDMA QUEUED
  25 00 00 00 08 00 02 3e 00 6a 98 40 00  4d+17:46:55.937  READ DMA EXT
  25 00 00 00 08 00 02 3e 00 6a 98 40 00  4d+17:46:55.937  READ DMA EXT
  25 00 00 00 08 00 02 3e 00 6a 98 40 00  4d+17:46:55.936  READ DMA EXT

SMART Extended Self-test Log Version: 1 (1 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       522 (0x020a)
Device State:                        Active (0)
Current Temperature:                    25 Celsius
Power Cycle Min/Max Temperature:     25/25 Celsius
Lifetime    Min/Max Temperature:     13/47 Celsius
Specified Max Operating Temperature:    33 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:     14/55 Celsius
Min/Max Temperature Limit:           10/60 Celsius
Temperature History Size (Index):    128 (127)

Index    Estimated Time   Temperature Celsius
   0    2021-08-14 20:32    40  *********************
 ...    ..( 15 skipped).    ..  *********************
  16    2021-08-14 20:48    40  *********************
  17    2021-08-14 20:49     ?  -
  18    2021-08-14 20:50    26  *******
  19    2021-08-14 20:51     ?  -
  20    2021-08-14 20:52    26  *******
  21    2021-08-14 20:53     ?  -
  22    2021-08-14 20:54    24  *****
  23    2021-08-14 20:55     ?  -
  24    2021-08-14 20:56    24  *****
  25    2021-08-14 20:57     ?  -
  26    2021-08-14 20:58    25  ******
  27    2021-08-14 20:59     ?  -
  28    2021-08-14 21:00    26  *******
  29    2021-08-14 21:01     ?  -
  30    2021-08-14 21:02    26  *******
  31    2021-08-14 21:03     ?  -
  32    2021-08-14 21:04    27  ********
  33    2021-08-14 21:05     ?  -
  34    2021-08-14 21:06    25  ******
  35    2021-08-14 21:07     ?  -
  36    2021-08-14 21:08    25  ******
  37    2021-08-14 21:09     ?  -
  38    2021-08-14 21:10    25  ******
  39    2021-08-14 21:11     ?  -
  40    2021-08-14 21:12    25  ******
  41    2021-08-14 21:13     ?  -
  42    2021-08-14 21:14    26  *******
  43    2021-08-14 21:15     ?  -
  44    2021-08-14 21:16    25  ******
  45    2021-08-14 21:17     ?  -
  46    2021-08-14 21:18    25  ******
  47    2021-08-14 21:19     ?  -
  48    2021-08-14 21:20    26  *******
  49    2021-08-14 21:21     ?  -
  50    2021-08-14 21:22    26  *******
  51    2021-08-14 21:23     ?  -
  52    2021-08-14 21:24    28  *********
  53    2021-08-14 21:25     ?  -
  54    2021-08-14 21:26    27  ********
  55    2021-08-14 21:27     ?  -
  56    2021-08-14 21:28    25  ******
  57    2021-08-14 21:29     ?  -
  58    2021-08-14 21:30    25  ******
  59    2021-08-14 21:31     ?  -
  60    2021-08-14 21:32    26  *******
  61    2021-08-14 21:33     ?  -
  62    2021-08-14 21:34    27  ********
  63    2021-08-14 21:35     ?  -
  64    2021-08-14 21:36    26  *******
  65    2021-08-14 21:37     ?  -
  66    2021-08-14 21:38    27  ********
  67    2021-08-14 21:39     ?  -
  68    2021-08-14 21:40    26  *******
  69    2021-08-14 21:41     ?  -
  70    2021-08-14 21:42    24  *****
  71    2021-08-14 21:43     ?  -
  72    2021-08-14 21:44    26  *******
  73    2021-08-14 21:45     ?  -
  74    2021-08-14 21:46    27  ********
  75    2021-08-14 21:47     ?  -
  76    2021-08-14 21:48    27  ********
  77    2021-08-14 21:49     ?  -
  78    2021-08-14 21:50    28  *********
  79    2021-08-14 21:51     ?  -
  80    2021-08-14 21:52    24  *****
  81    2021-08-14 21:53     ?  -
  82    2021-08-14 21:54    26  *******
  83    2021-08-14 21:55     ?  -
  84    2021-08-14 21:56    27  ********
  85    2021-08-14 21:57     ?  -
  86    2021-08-14 21:58    27  ********
  87    2021-08-14 21:59     ?  -
  88    2021-08-14 22:00    27  ********
  89    2021-08-14 22:01     ?  -
  90    2021-08-14 22:02    25  ******
  91    2021-08-14 22:03     ?  -
  92    2021-08-14 22:04    26  *******
  93    2021-08-14 22:05     ?  -
  94    2021-08-14 22:06    25  ******
  95    2021-08-14 22:07     ?  -
  96    2021-08-14 22:08    27  ********
  97    2021-08-14 22:09     ?  -
  98    2021-08-14 22:10    27  ********
  99    2021-08-14 22:11     ?  -
 100    2021-08-14 22:12    24  *****
 101    2021-08-14 22:13     ?  -
 102    2021-08-14 22:14    26  *******
 103    2021-08-14 22:15     ?  -
 104    2021-08-14 22:16    25  ******
 105    2021-08-14 22:17     ?  -
 106    2021-08-14 22:18    27  ********
 107    2021-08-14 22:19     ?  -
 108    2021-08-14 22:20    28  *********
 109    2021-08-14 22:21     ?  -
 110    2021-08-14 22:22    30  ***********
 111    2021-08-14 22:23    32  *************
 112    2021-08-14 22:24     ?  -
 113    2021-08-14 22:25    31  ************
 114    2021-08-14 22:26    33  **************
 115    2021-08-14 22:27     ?  -
 116    2021-08-14 22:28    16  -
 117    2021-08-14 22:29     ?  -
 118    2021-08-14 22:30    24  *****
 119    2021-08-14 22:31     ?  -
 120    2021-08-14 22:32    24  *****
 121    2021-08-14 22:33     ?  -
 122    2021-08-14 22:34    24  *****
 123    2021-08-14 22:35     ?  -
 124    2021-08-14 22:36    25  ******
 125    2021-08-14 22:37     ?  -
 126    2021-08-14 22:38    25  ******
 127    2021-08-14 22:39    25  ******

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x0001  2            0  Command failed due to ICRC error
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS

