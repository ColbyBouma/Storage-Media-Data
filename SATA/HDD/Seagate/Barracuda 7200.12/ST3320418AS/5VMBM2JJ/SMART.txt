smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.0-50-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Seagate Barracuda 7200.12
Device Model:     ST3320418AS
Serial Number:    5VMBM2JJ
LU WWN Device Id: 5 000c50 029e49260
Firmware Version: CC66
User Capacity:    320,072,933,376 bytes [320 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    7200 rpm
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS T13/1699-D revision 4
SATA Version is:  SATA 2.6, 3.0 Gb/s (current: 3.0 Gb/s)
Local Time is:    Tue May  4 12:01:24 2021 MDT

==> WARNING: A firmware update for this drive may be available,
see the following Seagate web pages:
http://knowledge.seagate.com/articles/en_US/FAQ/207931en
http://knowledge.seagate.com/articles/en_US/FAQ/213891en

SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Unknown

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x82)	Offline data collection activity
					was completed without error.
					Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(  625) seconds.
Offline data collection
capabilities: 			 (0x7b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (  68) minutes.
Conveyance self-test routine
recommended polling time: 	 (   2) minutes.
SCT capabilities: 	       (0x103f)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR--   114   099   006    -    64634699
  3 Spin_Up_Time            PO----   097   097   000    -    0
  4 Start_Stop_Count        -O--CK   100   100   020    -    893
  5 Reallocated_Sector_Ct   PO--CK   100   100   036    -    0
  7 Seek_Error_Rate         POSR--   081   060   030    -    122189730
  9 Power_On_Hours          -O--CK   085   085   000    -    13954
 10 Spin_Retry_Count        PO--C-   100   100   097    -    0
 12 Power_Cycle_Count       -O--CK   100   100   020    -    393
183 Runtime_Bad_Block       -O--CK   100   100   000    -    0
184 End-to-End_Error        -O--CK   100   100   099    -    0
187 Reported_Uncorrect      -O--CK   100   100   000    -    0
188 Command_Timeout         -O--CK   100   100   000    -    0
189 High_Fly_Writes         -O-RCK   100   100   000    -    0
190 Airflow_Temperature_Cel -O---K   074   052   045    -    26 (Min/Max 25/26)
194 Temperature_Celsius     -O---K   026   048   000    -    26 (0 18 0 0 0)
195 Hardware_ECC_Recovered  -O-RC-   066   039   000    -    64634699
197 Current_Pending_Sector  -O--C-   100   100   000    -    0
198 Offline_Uncorrectable   ----C-   100   100   000    -    0
199 UDMA_CRC_Error_Count    -OSRCK   200   200   000    -    0
240 Head_Flying_Hours       ------   100   253   000    -    15036 (121 59 0)
241 Total_LBAs_Written      ------   100   253   000    -    3204817543
242 Total_LBAs_Read         ------   100   253   000    -    3600101397
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01       GPL,SL  R/O      1  Summary SMART error log
0x02       GPL,SL  R/O      5  Comprehensive SMART error log
0x03       GPL     R/O      5  Ext. Comprehensive SMART error log
0x06       GPL,SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09       GPL,SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x21       GPL     R/O      1  Write stream error log
0x22       GPL     R/O      1  Read stream error log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xa1       GPL,SL  VS      20  Device vendor specific log
0xa2       GPL     VS    2248  Device vendor specific log
0xa8       GPL,SL  VS     129  Device vendor specific log
0xa9       GPL,SL  VS       1  Device vendor specific log
0xb0       GPL     VS    2928  Device vendor specific log
0xbd       GPL     VS     252  Device vendor specific log
0xbe-0xbf  GPL     VS   65535  Device vendor specific log
0xc0       GPL,SL  VS       1  Device vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (5 sectors)
No Errors Logged

SMART Extended Self-test Log Version: 1 (1 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Vendor (0x50)       Completed without error       00%         0         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       522 (0x020a)
Device State:                        Active (0)
Current Temperature:                    25 Celsius
Power Cycle Min/Max Temperature:     25/25 Celsius
Lifetime    Min/Max Temperature:     18/48 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        59 minutes
Min/Max recommended Temperature:     14/55 Celsius
Min/Max Temperature Limit:           10/60 Celsius
Temperature History Size (Index):    128 (23)

Index    Estimated Time   Temperature Celsius
  24    2021-04-29 06:18    39  ********************
 ...    ..(  4 skipped).    ..  ********************
  29    2021-04-29 11:13    39  ********************
  30    2021-04-29 12:12    40  *********************
  31    2021-04-29 13:11    40  *********************
  32    2021-04-29 14:10    39  ********************
  33    2021-04-29 15:09    38  *******************
 ...    ..(  5 skipped).    ..  *******************
  39    2021-04-29 21:03    38  *******************
  40    2021-04-29 22:02    39  ********************
 ...    ..( 10 skipped).    ..  ********************
  51    2021-04-30 08:51    39  ********************
  52    2021-04-30 09:50    38  *******************
  53    2021-04-30 10:49    39  ********************
 ...    ..( 16 skipped).    ..  ********************
  70    2021-05-01 03:32    39  ********************
  71    2021-05-01 04:31    38  *******************
  72    2021-05-01 05:30    39  ********************
 ...    ..( 17 skipped).    ..  ********************
  90    2021-05-01 23:12    39  ********************
  91    2021-05-02 00:11     ?  -
  92    2021-05-02 01:10    38  *******************
  93    2021-05-02 02:09    38  *******************
  94    2021-05-02 03:08    39  ********************
 ...    ..( 12 skipped).    ..  ********************
 107    2021-05-02 15:55    39  ********************
 108    2021-05-02 16:54    42  ***********************
 109    2021-05-02 17:53    39  ********************
 ...    ..(  4 skipped).    ..  ********************
 114    2021-05-02 22:48    39  ********************
 115    2021-05-02 23:47     ?  -
 116    2021-05-03 00:46    22  ***
 117    2021-05-03 01:45     ?  -
 118    2021-05-03 02:44    23  ****
 119    2021-05-03 03:43     ?  -
 120    2021-05-03 04:42    23  ****
 121    2021-05-03 05:41     ?  -
 122    2021-05-03 06:40    27  ********
 123    2021-05-03 07:39     ?  -
 124    2021-05-03 08:38    23  ****
 125    2021-05-03 09:37    23  ****
 126    2021-05-03 10:36    27  ********
 127    2021-05-03 11:35    32  *************
 ...    ..(  4 skipped).    ..  *************
   4    2021-05-03 16:30    32  *************
   5    2021-05-03 17:29    31  ************
 ...    ..(  8 skipped).    ..  ************
  14    2021-05-04 02:20    31  ************
  15    2021-05-04 03:19    33  **************
  16    2021-05-04 04:18    35  ****************
 ...    ..(  3 skipped).    ..  ****************
  20    2021-05-04 08:14    35  ****************
  21    2021-05-04 09:13     ?  -
  22    2021-05-04 10:12    25  ******
  23    2021-05-04 11:11    25  ******

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x0001  2            0  Command failed due to ICRC error
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS

