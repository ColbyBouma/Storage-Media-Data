smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.11.0-25-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Hitachi/HGST Travelstar 5K750
Device Model:     APPLE HDD HTS547550A9E384
Serial Number:    J226004MGJ5S4D
LU WWN Device Id: 5 000cca 726c75b82
Firmware Version: JE3AD70F
User Capacity:    500,107,862,016 bytes [500 GB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5400 rpm
Form Factor:      2.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 2.6, 3.0 Gb/s
Local Time is:    Fri Aug 13 15:56:13 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(   45) seconds.
Offline data collection
capabilities: 			 (0x5b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 130) minutes.
SCT capabilities: 	       (0x003d)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     PO-R--   099   099   062    -    131072
  2 Throughput_Performance  P-S---   100   100   040    -    0
  3 Spin_Up_Time            POS---   173   173   033    -    1
  4 Start_Stop_Count        -O--C-   092   092   000    -    13192
  5 Reallocated_Sector_Ct   PO--CK   098   098   005    -    249
  7 Seek_Error_Rate         PO-R--   100   100   067    -    0
  8 Seek_Time_Performance   P-S---   100   100   040    -    0
  9 Power_On_Hours          -O--C-   086   086   000    -    6252
 10 Spin_Retry_Count        PO--C-   100   100   060    -    0
 12 Power_Cycle_Count       -O--CK   096   096   000    -    6593
160 Unknown_Attribute       -O--CK   100   100   000    -    0
191 G-Sense_Error_Rate      -O-R--   100   100   000    -    0
192 Power-Off_Retract_Count -O--CK   097   097   000    -    3036541878357
193 Load_Cycle_Count        -O--C-   049   049   000    -    517264
194 Temperature_Celsius     -O----   250   250   000    -    24 (Min/Max 15/39)
195 Hardware_ECC_Recovered  -O-R--   100   100   000    -    0
196 Reallocated_Event_Count -O--CK   083   083   000    -    617
197 Current_Pending_Sector  -O---K   100   100   000    -    8
198 Offline_Uncorrectable   ---R--   100   100   000    -    0
199 UDMA_CRC_Error_Count    -O-R--   200   200   000    -    0
223 Load_Retry_Count        -O-R--   100   100   000    -    0
254 Free_Fall_Sensor        -O--CK   059   059   000    -    10751
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      1  Comprehensive SMART error log
0x03       GPL     R/O      1  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (1 sectors)
Device Error Count: 1
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 1 [0] occurred at disk power-on lifetime: 6252 hours (260 days + 12 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 01 00 00 00 00 00 00 00 00  Error: UNC at LBA = 0x00000000 = 0

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  42 00 00 00 01 00 00 00 00 00 00 e0 00     00:00:29.732  READ VERIFY SECTOR(S) EXT
  ef 82 55 00 00 00 00 00 00 00 00 e0 00     00:00:29.729  SET FEATURES [Disable read look-ahead]
  ef 00 82 01 00 00 00 01 00 00 00 e0 00     00:00:29.727  SET FEATURES [Disable write cache]
  90 86 00 00 00 00 00 00 00 00 00 e0 00     00:00:29.707  EXECUTE DEVICE DIAGNOSTIC
  ef 00 86 01 00 00 00 01 00 00 00 e0 00     00:00:29.654  SET FEATURES [Disable Pwr-Up In Standby]

SMART Extended Self-test Log Version: 1 (1 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed: read failure       90%      6252         679338165

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       256 (0x0100)
Device State:                        Active (0)
Current Temperature:                    24 Celsius
Power Cycle Min/Max Temperature:     24/24 Celsius
Lifetime    Min/Max Temperature:     15/39 Celsius
Specified Max Operating Temperature:    27 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      0/60 Celsius
Min/Max Temperature Limit:           -40/65 Celsius
Temperature History Size (Index):    128 (14)

Index    Estimated Time   Temperature Celsius
  15    2021-08-13 13:49    28  *********
  16    2021-08-13 13:50    29  **********
  17    2021-08-13 13:51    29  **********
  18    2021-08-13 13:52    29  **********
  19    2021-08-13 13:53    30  ***********
 ...    ..(  4 skipped).    ..  ***********
  24    2021-08-13 13:58    30  ***********
  25    2021-08-13 13:59    31  ************
  26    2021-08-13 14:00    30  ***********
  27    2021-08-13 14:01    31  ************
 ...    ..(  3 skipped).    ..  ************
  31    2021-08-13 14:05    31  ************
  32    2021-08-13 14:06    32  *************
  33    2021-08-13 14:07     ?  -
  34    2021-08-13 14:08    22  ***
  35    2021-08-13 14:09     ?  -
  36    2021-08-13 14:10    21  **
  37    2021-08-13 14:11     ?  -
  38    2021-08-13 14:12    22  ***
  39    2021-08-13 14:13     ?  -
  40    2021-08-13 14:14    23  ****
  41    2021-08-13 14:15     ?  -
  42    2021-08-13 14:16     ?  -
  43    2021-08-13 14:17    23  ****
  44    2021-08-13 14:18     ?  -
  45    2021-08-13 14:19     ?  -
  46    2021-08-13 14:20    24  *****
  47    2021-08-13 14:21    25  ******
  48    2021-08-13 14:22    25  ******
  49    2021-08-13 14:23    27  ********
  50    2021-08-13 14:24    27  ********
  51    2021-08-13 14:25    28  *********
 ...    ..(  3 skipped).    ..  *********
  55    2021-08-13 14:29    28  *********
  56    2021-08-13 14:30    29  **********
 ...    ..(  5 skipped).    ..  **********
  62    2021-08-13 14:36    29  **********
  63    2021-08-13 14:37    30  ***********
 ...    ..( 10 skipped).    ..  ***********
  74    2021-08-13 14:48    30  ***********
  75    2021-08-13 14:49    31  ************
  76    2021-08-13 14:50    31  ************
  77    2021-08-13 14:51     ?  -
  78    2021-08-13 14:52    26  *******
  79    2021-08-13 14:53    27  ********
 ...    ..(  2 skipped).    ..  ********
  82    2021-08-13 14:56    27  ********
  83    2021-08-13 14:57    28  *********
 ...    ..(  5 skipped).    ..  *********
  89    2021-08-13 15:03    28  *********
  90    2021-08-13 15:04    29  **********
 ...    ..( 11 skipped).    ..  **********
 102    2021-08-13 15:16    29  **********
 103    2021-08-13 15:17    30  ***********
 ...    ..( 11 skipped).    ..  ***********
 115    2021-08-13 15:29    30  ***********
 116    2021-08-13 15:30    31  ************
 117    2021-08-13 15:31    30  ***********
 118    2021-08-13 15:32    30  ***********
 119    2021-08-13 15:33    31  ************
 ...    ..(  7 skipped).    ..  ************
 127    2021-08-13 15:41    31  ************
   0    2021-08-13 15:42    32  *************
   1    2021-08-13 15:43     ?  -
   2    2021-08-13 15:44    22  ***
   3    2021-08-13 15:45     ?  -
   4    2021-08-13 15:46    24  *****
   5    2021-08-13 15:47    24  *****
   6    2021-08-13 15:48    25  ******
   7    2021-08-13 15:49    25  ******
   8    2021-08-13 15:50     ?  -
 ...    ..(  5 skipped).    ..  -
  14    2021-08-13 15:56     ?  -

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0009  2            0  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            0  Device-to-host register FISes sent due to a COMRESET
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS

