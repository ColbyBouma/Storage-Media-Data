smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.11.0-25-generic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     HGST Travelstar 7K1000
Device Model:     HGST HTS721010A9E630
Serial Number:    JR1000D325ZV2E
LU WWN Device Id: 5 000cca 8c8dee8a3
Firmware Version: JB0OA3T0
User Capacity:    1,000,204,886,016 bytes [1.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    7200 rpm
Form Factor:      2.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ACS-2, ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Sat Aug 21 22:48:25 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: FAILED!
Drive failure expected in less than 24 hours. SAVE ALL DATA.
See vendor-specific Attribute list for failed Attributes.

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(   45) seconds.
Offline data collection
capabilities: 			 (0x5b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 168) minutes.
SCT capabilities: 	       (0x003d)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   098   098   062    -    262144
  2 Throughput_Performance  P-S--K   100   100   040    -    0
  3 Spin_Up_Time            PO---K   122   100   033    -    2
  4 Start_Stop_Count        -O--CK   100   100   000    -    16
  5 Reallocated_Sector_Ct   PO--CK   039   027   005    -    33672 (0 1496)
  7 Seek_Error_Rate         POSR-K   028   028   067    NOW  1954
  8 Seek_Time_Performance   P-S--K   100   100   040    -    0
  9 Power_On_Hours          -O--CK   100   100   000    -    7
 10 Spin_Retry_Count        PO--CK   100   100   060    -    0
 12 Power_Cycle_Count       -O--CK   100   100   000    -    16
183 Runtime_Bad_Block       -O--CK   100   100   000    -    0
184 End-to-End_Error        PO--CK   100   100   097    -    0
187 Reported_Uncorrect      -O--CK   100   100   000    -    3342336
188 Command_Timeout         -O--CK   100   100   000    -    21475360768
190 Airflow_Temperature_Cel -O---K   078   060   045    -    22 (Min/Max 21/22)
191 G-Sense_Error_Rate      -O--CK   100   100   000    -    1
192 Power-Off_Retract_Count -O--CK   100   100   000    -    1048592
193 Load_Cycle_Count        -O--CK   100   100   000    -    32
196 Reallocated_Event_Count -O--CK   001   001   000    -    3624
197 Current_Pending_Sector  -O--CK   100   001   000    -    40
198 Offline_Uncorrectable   ----CK   100   100   000    -    0
199 UDMA_CRC_Error_Count    -OS-CK   100   100   000    -    0
223 Load_Retry_Count        -O-R-K   100   100   000    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      1  Comprehensive SMART error log
0x03       GPL     R/O      1  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (1 sectors)
Device Error Count: 49087 (device log contains only the most recent 4 errors)
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 49087 [2] occurred at disk power-on lifetime: 4456 hours (185 days + 16 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 99 00 00 4f a6 a3 c0 0f 00  Error: UNC at LBA = 0x4fa6a3c0 = 1336320960

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 04 00 00 80 00 00 4f a6 6c 59 40 00     00:36:07.858  READ FPDMA QUEUED
  60 04 00 00 78 00 00 4f a6 70 59 40 00     00:36:07.858  READ FPDMA QUEUED
  60 04 00 00 70 00 00 4f a6 74 59 40 00     00:36:07.858  READ FPDMA QUEUED
  60 04 00 00 68 00 00 4f a6 78 59 40 00     00:36:07.858  READ FPDMA QUEUED
  60 04 00 00 60 00 00 4f a6 7c 59 40 00     00:36:07.858  READ FPDMA QUEUED

Error 49086 [1] occurred at disk power-on lifetime: 4456 hours (185 days + 16 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 19 00 00 4f a6 6c 40 0f 00  Error: UNC at LBA = 0x4fa66c40 = 1336306752

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 04 00 00 90 00 00 4f a6 58 59 40 00     00:36:05.092  READ FPDMA QUEUED
  60 04 00 00 88 00 00 4f a6 ac 59 40 00     00:36:05.092  READ FPDMA QUEUED
  60 04 00 00 80 00 00 4f a6 a8 59 40 00     00:36:05.092  READ FPDMA QUEUED
  60 04 00 00 78 00 00 4f a6 a4 59 40 00     00:36:05.092  READ FPDMA QUEUED
  60 04 00 00 70 00 00 4f a6 a0 59 40 00     00:36:05.092  READ FPDMA QUEUED

Error 49085 [0] occurred at disk power-on lifetime: 4456 hours (185 days + 16 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 03 39 00 00 4f a6 51 20 0f 00  Error: UNC at LBA = 0x4fa65120 = 1336299808

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 04 00 00 a0 00 00 4f a6 68 59 40 00     00:36:02.283  READ FPDMA QUEUED
  60 04 00 00 98 00 00 4f a6 6c 59 40 00     00:36:02.283  READ FPDMA QUEUED
  60 04 00 00 90 00 00 4f a6 70 59 40 00     00:36:02.283  READ FPDMA QUEUED
  60 04 00 00 88 00 00 4f a6 74 59 40 00     00:36:02.283  READ FPDMA QUEUED
  60 04 00 00 80 00 00 4f a6 78 59 40 00     00:36:02.283  READ FPDMA QUEUED

Error 49084 [3] occurred at disk power-on lifetime: 4456 hours (185 days + 16 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 02 e9 00 00 4f a6 65 70 0f 00  Error: UNC at LBA = 0x4fa66570 = 1336305008

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 04 00 00 b0 00 00 4f a6 50 59 40 00     00:35:59.450  READ FPDMA QUEUED
  60 04 00 00 a8 00 00 4f a6 4c 59 40 00     00:35:59.450  READ FPDMA QUEUED
  60 04 00 00 a0 00 00 4f a6 58 59 40 00     00:35:59.450  READ FPDMA QUEUED
  60 04 00 00 98 00 00 4f a6 ac 59 40 00     00:35:59.450  READ FPDMA QUEUED
  60 04 00 00 90 00 00 4f a6 a8 59 40 00     00:35:59.450  READ FPDMA QUEUED

SMART Extended Self-test Log Version: 1 (1 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed: read failure       90%      4349         119957688
# 2  Short offline       Completed without error       00%      1233         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       256 (0x0100)
Device State:                        Active (0)
Current Temperature:                    22 Celsius
Power Cycle Min/Max Temperature:     21/22 Celsius
Lifetime    Min/Max Temperature:     19/40 Celsius
Specified Max Operating Temperature:    38 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      0/60 Celsius
Min/Max Temperature Limit:           -40/65 Celsius
Temperature History Size (Index):    128 (4)

Index    Estimated Time   Temperature Celsius
   5    2021-08-21 20:41    39  ********************
   6    2021-08-21 20:42    38  *******************
 ...    ..(122 skipped).    ..  *******************
   1    2021-08-21 22:45    38  *******************
   2    2021-08-21 22:46     ?  -
   3    2021-08-21 22:47     ?  -
   4    2021-08-21 22:48     ?  -

SCT Error Recovery Control:
           Read:     85 (8.5 seconds)
          Write:     85 (8.5 seconds)

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0009  2            1  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            0  Device-to-host register FISes sent due to a COMRESET
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS

