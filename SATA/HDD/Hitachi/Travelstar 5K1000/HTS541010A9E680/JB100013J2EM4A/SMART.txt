smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.11.0-25-generic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     Hitachi HTS541010A9E680
Serial Number:    JB100013J2EM4A
LU WWN Device Id: 5 000cca 6a0dd4ade
Firmware Version: JA0OA4D0
User Capacity:    1,000,204,886,016 bytes [1.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5400 rpm
Form Factor:      2.5 inches
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   ACS-2, ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 3.0 Gb/s (current: 3.0 Gb/s)
Local Time is:    Sat Aug 21 23:23:21 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(   45) seconds.
Offline data collection
capabilities: 			 (0x5b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 229) minutes.
SCT capabilities: 	       (0x003d)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   085   085   062    -    10747945
  2 Throughput_Performance  P-S--K   100   100   040    -    0
  3 Spin_Up_Time            PO---K   172   100   033    -    1
  4 Start_Stop_Count        -O--CK   096   096   000    -    7490
  5 Reallocated_Sector_Ct   PO--CK   060   060   005    -    64216 (0 1095)
  7 Seek_Error_Rate         POSR-K   100   100   067    -    0
  8 Seek_Time_Performance   P-S--K   100   100   040    -    0
  9 Power_On_Hours          -O--CK   099   099   000    -    486
 10 Spin_Retry_Count        PO--CK   100   100   060    -    0
 12 Power_Cycle_Count       -O--CK   096   096   000    -    7486
183 Runtime_Bad_Block       -O--CK   100   100   000    -    0
184 End-to-End_Error        PO--CK   100   100   097    -    0
187 Reported_Uncorrect      -O--CK   001   001   000    -    37271726588026
188 Command_Timeout         -O--CK   099   099   000    -    2097154
190 Airflow_Temperature_Cel -O---K   080   047   045    -    20 (Min/Max 20/20)
191 G-Sense_Error_Rate      -O--CK   099   099   000    -    383
192 Power-Off_Retract_Count -O--CK   068   068   000    -    423631168
193 Load_Cycle_Count        -O--CK   098   098   000    -    27584
196 Reallocated_Event_Count -O--CK   001   001   000    -    6975
197 Current_Pending_Sector  -O--CK   100   100   000    -    48
198 Offline_Uncorrectable   ----CK   100   100   000    -    0
199 UDMA_CRC_Error_Count    -OS-CK   100   100   000    -    0
223 Load_Retry_Count        -O-R-K   100   100   000    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      1  Comprehensive SMART error log
0x03       GPL     R/O      1  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (1 sectors)
Device Error Count: 122 (device log contains only the most recent 4 errors)
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 122 [1] occurred at disk power-on lifetime: 458 hours (19 days + 2 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 28 00 00 00 00 08 f0 00 00  Error: UNC 40 sectors at LBA = 0x000008f0 = 2288

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 01 00 00 00 00 00 08 18 40 00     00:10:00.547  READ DMA EXT
  25 00 00 00 01 00 00 00 00 00 00 40 00     00:10:00.463  READ DMA EXT
  25 00 00 00 01 00 00 00 00 00 00 40 00     00:10:00.462  READ DMA EXT
  25 00 00 00 20 00 00 00 00 00 02 40 00     00:10:00.462  READ DMA EXT
  25 00 00 00 01 00 00 00 00 00 01 40 00     00:10:00.462  READ DMA EXT

Error 121 [0] occurred at disk power-on lifetime: 458 hours (19 days + 2 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 28 00 00 00 00 08 f0 00 00  Error: UNC 40 sectors at LBA = 0x000008f0 = 2288

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 03 01 00 00 00 00 00 08 18 40 00     00:09:39.548  READ DMA EXT
  ef 00 03 01 46 00 00 00 00 08 18 00 00     00:09:39.548  SET FEATURES [Set transfer mode]
  ef 00 03 01 0c 00 00 00 00 08 18 00 00     00:09:39.548  SET FEATURES [Set transfer mode]
  ec 00 d1 01 00 00 00 00 00 08 18 00 00     00:09:39.546  IDENTIFY DEVICE
  25 00 d1 00 28 00 00 00 00 08 f0 00 ff     00:09:38.547  READ DMA EXT

Error 120 [3] occurred at disk power-on lifetime: 458 hours (19 days + 2 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 28 00 00 00 00 08 f0 00 00  Error: UNC 40 sectors at LBA = 0x000008f0 = 2288

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 d1 01 00 00 00 00 00 08 18 40 00     00:09:18.545  READ DMA EXT
  b0 00 d1 01 01 00 00 00 c2 4f 00 00 00     00:09:17.660  SMART READ ATTRIBUTE THRESHOLDS [OBS-4]
  ef 00 03 01 46 00 00 00 00 08 18 00 00     00:09:17.660  SET FEATURES [Set transfer mode]
  ef 00 03 01 0c 00 00 00 00 08 18 00 00     00:09:17.660  SET FEATURES [Set transfer mode]
  ec 00 d0 01 00 00 00 00 00 08 18 00 00     00:09:17.658  IDENTIFY DEVICE

Error 119 [2] occurred at disk power-on lifetime: 458 hours (19 days + 2 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 28 00 00 00 00 08 f0 00 00  Error: UNC 40 sectors at LBA = 0x000008f0 = 2288

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 d0 01 00 00 00 00 00 08 18 40 00     00:08:57.544  READ DMA EXT
  b0 00 d0 01 01 00 00 00 c2 4f 00 00 00     00:08:57.520  SMART READ DATA
  b0 00 d8 01 01 00 00 00 c2 4f 00 00 00     00:08:57.066  SMART ENABLE OPERATIONS
  b0 00 da 01 01 00 00 00 c2 4f 00 00 00     00:08:57.065  SMART RETURN STATUS
  ec 00 00 01 01 00 00 00 00 00 00 00 00     00:08:57.063  IDENTIFY DEVICE

SMART Extended Self-test Log Version: 1 (1 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Extended captive    Completed: read failure       90%       477         13612776
# 2  Extended captive    Completed: read failure       90%       458         2288

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       256 (0x0100)
Device State:                        Active (0)
Current Temperature:                    20 Celsius
Power Cycle Min/Max Temperature:     20/20 Celsius
Lifetime    Min/Max Temperature:      8/53 Celsius
Specified Max Operating Temperature:    31 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      0/60 Celsius
Min/Max Temperature Limit:           -40/65 Celsius
Temperature History Size (Index):    128 (126)

Index    Estimated Time   Temperature Celsius
 127    2021-08-21 21:16     ?  -
 ...    ..(101 skipped).    ..  -
 101    2021-08-21 22:58     ?  -
 102    2021-08-21 22:59    29  **********
 103    2021-08-21 23:00     ?  -
 104    2021-08-21 23:01     ?  -
 105    2021-08-21 23:02     ?  -
 106    2021-08-21 23:03    35  ****************
 107    2021-08-21 23:04    35  ****************
 108    2021-08-21 23:05    35  ****************
 109    2021-08-21 23:06    36  *****************
 110    2021-08-21 23:07    36  *****************
 111    2021-08-21 23:08    36  *****************
 112    2021-08-21 23:09    37  ******************
 113    2021-08-21 23:10    37  ******************
 114    2021-08-21 23:11     ?  -
 ...    ..( 11 skipped).    ..  -
 126    2021-08-21 23:23     ?  -

SCT Error Recovery Control:
           Read:     85 (8.5 seconds)
          Write:     85 (8.5 seconds)

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0009  2            0  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            0  Device-to-host register FISes sent due to a COMRESET
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS

