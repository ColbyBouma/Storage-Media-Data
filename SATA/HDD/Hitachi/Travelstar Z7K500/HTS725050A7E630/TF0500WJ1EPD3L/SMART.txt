smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.11.0-25-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Hitachi/HGST Travelstar Z7K500
Device Model:     HGST HTS725050A7E630
Serial Number:    TF0500WJ1EPD3L
LU WWN Device Id: 5 000cca 7c5d450a0
Firmware Version: GH2OA910
User Capacity:    500,107,862,016 bytes [500 GB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    7200 rpm
Form Factor:      2.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ACS-2, ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Fri Aug 13 19:35:23 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(   45) seconds.
Offline data collection
capabilities: 			 (0x5b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 (  92) minutes.
SCT capabilities: 	       (0x003d)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   100   100   062    -    0
  2 Throughput_Performance  P-S--K   100   100   040    -    0
  3 Spin_Up_Time            PO---K   253   100   033    -    1
  4 Start_Stop_Count        -O--CK   100   100   000    -    15
  5 Reallocated_Sector_Ct   PO--CK   100   100   005    -    120 (0 14)
  7 Seek_Error_Rate         POSR-K   100   100   067    -    0
  8 Seek_Time_Performance   P-S--K   100   100   040    -    0
  9 Power_On_Hours          -O--CK   100   100   000    -    1
 10 Spin_Retry_Count        PO--CK   100   100   060    -    0
 12 Power_Cycle_Count       -O--CK   100   100   000    -    6
183 Runtime_Bad_Block       -O--CK   100   100   000    -    0
184 End-to-End_Error        PO--CK   100   100   097    -    0
187 Reported_Uncorrect      -O--CK   100   100   000    -    0
188 Command_Timeout         -O--CK   100   100   000    -    0
190 Airflow_Temperature_Cel -O---K   076   065   045    -    24 (Min/Max 24/24)
191 G-Sense_Error_Rate      -O--CK   100   100   000    -    0
192 Power-Off_Retract_Count -O--CK   100   100   000    -    196611
193 Load_Cycle_Count        -O--CK   100   100   000    -    24
196 Reallocated_Event_Count -O--CK   100   100   000    -    14
197 Current_Pending_Sector  -O--CK   100   100   000    -    0
198 Offline_Uncorrectable   ----CK   100   100   000    -    0
199 UDMA_CRC_Error_Count    -OS-CK   100   100   000    -    0
223 Load_Retry_Count        -O-R-K   100   100   000    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      1  Comprehensive SMART error log
0x03       GPL     R/O      1  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (1 sectors)
Device Error Count: 78 (device log contains only the most recent 4 errors)
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 78 [1] occurred at disk power-on lifetime: 319 hours (13 days + 7 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 40 00 00 00 57 c6 53 00 00  Error: WP at LBA = 0x0057c653 = 5752403

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  61 00 08 00 70 00 00 00 53 e9 c0 40 00     00:33:36.836  WRITE FPDMA QUEUED
  60 00 01 00 68 00 00 00 56 d9 53 40 00     00:33:36.822  READ FPDMA QUEUED
  60 00 40 00 60 00 00 00 3d ad c5 40 00     00:33:36.814  READ FPDMA QUEUED
  60 00 40 00 58 00 00 00 57 c6 53 40 00     00:33:36.799  READ FPDMA QUEUED
  61 00 08 00 50 00 00 00 2f 3e d0 40 00     00:33:34.167  WRITE FPDMA QUEUED

Error 77 [0] occurred at disk power-on lifetime: 319 hours (13 days + 7 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 08 00 00 00 2e e0 10 00 00  Error: WP at LBA = 0x002ee010 = 3072016

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  61 00 08 00 28 00 00 00 32 1a 10 40 00     00:33:28.155  WRITE FPDMA QUEUED
  61 00 08 00 20 00 00 04 41 3c 30 40 00     00:33:28.154  WRITE FPDMA QUEUED
  60 00 20 00 18 00 00 00 3d 96 ea 40 00     00:33:19.483  READ FPDMA QUEUED
  61 00 08 00 10 00 00 1d 32 54 18 40 00     00:33:19.468  WRITE FPDMA QUEUED
  61 00 08 00 08 00 00 1d 32 54 10 40 00     00:33:19.467  WRITE FPDMA QUEUED

Error 76 [3] occurred at disk power-on lifetime: 319 hours (13 days + 7 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 08 00 00 05 c0 dc 18 05 00  Error: WP at LBA = 0x05c0dc18 = 96525336

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  61 00 08 00 68 00 00 00 3e 50 60 40 00     00:32:47.158  WRITE FPDMA QUEUED
  61 00 80 00 60 00 00 03 5c 41 f0 40 00     00:32:39.667  WRITE FPDMA QUEUED
  61 00 08 00 58 00 00 04 41 3c 20 40 00     00:32:39.667  WRITE FPDMA QUEUED
  61 00 08 00 50 00 00 05 b6 9c 48 40 00     00:32:39.667  WRITE FPDMA QUEUED
  61 00 80 00 48 00 00 03 5c 41 70 40 00     00:32:39.667  WRITE FPDMA QUEUED

Error 75 [2] occurred at disk power-on lifetime: 319 hours (13 days + 7 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 08 00 00 05 c0 dc 18 05 00  Error: WP at LBA = 0x05c0dc18 = 96525336

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  61 00 08 00 98 00 00 00 43 42 58 40 00     00:32:27.160  WRITE FPDMA QUEUED
  61 00 10 00 90 00 00 1d 32 dc 18 40 00     00:32:25.164  WRITE FPDMA QUEUED
  61 00 08 00 88 00 00 00 30 d1 b8 40 00     00:32:25.163  WRITE FPDMA QUEUED
  61 00 80 00 80 00 00 01 f4 92 f8 40 00     00:32:23.168  WRITE FPDMA QUEUED
  60 00 40 00 78 00 00 00 5b 46 33 40 00     00:32:23.167  READ FPDMA QUEUED

SMART Extended Self-test Log Version: 1 (1 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed without error       00%       320         -
# 2  Short offline       Completed without error       00%        35         -
# 3  Extended offline    Completed without error       00%         4         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       256 (0x0100)
Device State:                        Active (0)
Current Temperature:                    24 Celsius
Power Cycle Min/Max Temperature:     24/24 Celsius
Lifetime    Min/Max Temperature:     23/35 Celsius
Specified Max Operating Temperature:    33 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      0/60 Celsius
Min/Max Temperature Limit:           -40/65 Celsius
Temperature History Size (Index):    128 (66)

Index    Estimated Time   Temperature Celsius
  67    2021-08-13 17:28    34  ***************
  68    2021-08-13 17:29    33  **************
  69    2021-08-13 17:30    34  ***************
  70    2021-08-13 17:31    34  ***************
  71    2021-08-13 17:32    33  **************
  72    2021-08-13 17:33    34  ***************
  73    2021-08-13 17:34    33  **************
  74    2021-08-13 17:35    34  ***************
  75    2021-08-13 17:36    33  **************
  76    2021-08-13 17:37    33  **************
  77    2021-08-13 17:38    33  **************
  78    2021-08-13 17:39    34  ***************
  79    2021-08-13 17:40    34  ***************
  80    2021-08-13 17:41    33  **************
 ...    ..( 21 skipped).    ..  **************
 102    2021-08-13 18:03    33  **************
 103    2021-08-13 18:04     ?  -
 104    2021-08-13 18:05     ?  -
 105    2021-08-13 18:06    33  **************
 106    2021-08-13 18:07    33  **************
 107    2021-08-13 18:08     ?  -
 ...    ..(  2 skipped).    ..  -
 110    2021-08-13 18:11     ?  -
 111    2021-08-13 18:12    31  ************
 112    2021-08-13 18:13    32  *************
 113    2021-08-13 18:14    32  *************
 114    2021-08-13 18:15    33  **************
 115    2021-08-13 18:16    33  **************
 116    2021-08-13 18:17    34  ***************
 ...    ..(  3 skipped).    ..  ***************
 120    2021-08-13 18:21    34  ***************
 121    2021-08-13 18:22    35  ****************
 122    2021-08-13 18:23    34  ***************
 123    2021-08-13 18:24    34  ***************
 124    2021-08-13 18:25    34  ***************
 125    2021-08-13 18:26    35  ****************
 126    2021-08-13 18:27    35  ****************
 127    2021-08-13 18:28    35  ****************
   0    2021-08-13 18:29    34  ***************
 ...    ..( 54 skipped).    ..  ***************
  55    2021-08-13 19:24    34  ***************
  56    2021-08-13 19:25    33  **************
  57    2021-08-13 19:26    34  ***************
  58    2021-08-13 19:27    34  ***************
  59    2021-08-13 19:28    33  **************
  60    2021-08-13 19:29    34  ***************
  61    2021-08-13 19:30    34  ***************
  62    2021-08-13 19:31    34  ***************
  63    2021-08-13 19:32    33  **************
  64    2021-08-13 19:33     ?  -
  65    2021-08-13 19:34     ?  -
  66    2021-08-13 19:35     ?  -

SCT Error Recovery Control:
           Read:     85 (8.5 seconds)
          Write:     85 (8.5 seconds)

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0009  2            1  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            0  Device-to-host register FISes sent due to a COMRESET
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS

