smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.0-50-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Maxtor DiamondMax Plus 9
Device Model:     Maxtor 6Y080M0
Serial Number:    Y2L1FSJE
Firmware Version: YAR51HW0
User Capacity:    80,000,000,000 bytes [80.0 GB]
Sector Size:      512 bytes logical/physical
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA/ATAPI-7 T13/1532D revision 0
Local Time is:    Tue May  4 11:32:14 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM level is:     254 (maximum performance), recommended: 192
APM level is:     254 (maximum performance)
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Unavailable

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED
See vendor-specific Attribute list for marginal Attributes.

General SMART Values:
Offline data collection status:  (0x80)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(  241) seconds.
Offline data collection
capabilities: 			 (0x5b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					No General Purpose Logging support.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 (  36) minutes.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  3 Spin_Up_Time            POS--K   226   225   063    -    6765
  4 Start_Stop_Count        -O--CK   253   253   000    -    509
  5 Reallocated_Sector_Ct   PO--CK   243   001   063    Past 104
  6 Read_Channel_Margin     P-----   253   253   100    -    0
  7 Seek_Error_Rate         -O-R--   253   252   000    -    0
  8 Seek_Time_Performance   POS--K   253   241   187    -    56021
  9 Power_On_Minutes        -O--CK   246   246   000    -    312h+31m
 10 Spin_Retry_Count        PO-R-K   253   252   157    -    0
 11 Calibration_Retry_Count PO-R-K   253   252   223    -    0
 12 Power_Cycle_Count       -O--CK   252   252   000    -    519
192 Power-Off_Retract_Count -O--CK   253   253   000    -    0
193 Load_Cycle_Count        -O--CK   253   253   000    -    0
194 Temperature_Celsius     -O--CK   253   253   000    -    21
195 Hardware_ECC_Recovered  -O-R--   253   252   000    -    3175
196 Reallocated_Event_Count ---R--   223   223   000    -    30
197 Current_Pending_Sector  ---R--   232   001   000    -    218
198 Offline_Uncorrectable   ---R--   001   001   000    -    7220
199 UDMA_CRC_Error_Count    ---R--   199   199   000    -    0
200 Multi_Zone_Error_Rate   -O-R--   253   252   000    -    0
201 Soft_Read_Error_Rate    -O-R--   253   252   000    -    8
202 Data_Address_Mark_Errs  -O-R--   252   231   000    -    3
203 Run_Out_Cancel          PO-R--   251   243   180    -    2387
204 Soft_ECC_Correction     -O-R--   253   216   000    -    1
205 Thermal_Asperity_Rate   -O-R--   253   252   000    -    0
207 Spin_High_Current       -O-R-K   253   252   000    -    0
208 Spin_Buzz               -O-R-K   253   252   000    -    0
209 Offline_Seek_Performnce --S--K   195   195   000    -    0
 99 Unknown_Attribute       --S---   253   253   000    -    0
100 Unknown_Attribute       --S---   253   253   000    -    0
101 Unknown_Attribute       --S---   253   253   000    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

Read SMART Log Directory failed: scsi error badly formed scsi parameters

General Purpose Log Directory not supported

SMART Extended Comprehensive Error Log (GP Log 0x03) not supported

SMART Error Log Version: 1
Warning: ATA error count 12703 inconsistent with error log pointer 5

ATA Error Count: 12703 (device log contains only the most recent five errors)
	CR = Command Register [HEX]
	FR = Features Register [HEX]
	SC = Sector Count Register [HEX]
	SN = Sector Number Register [HEX]
	CL = Cylinder Low Register [HEX]
	CH = Cylinder High Register [HEX]
	DH = Device/Head Register [HEX]
	DC = Device Command Register [HEX]
	ER = Error register [HEX]
	ST = Status register [HEX]
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 12703 occurred at disk power-on lifetime: 2348 hours (97 days + 20 hours)
  When the command that caused the error occurred, the device was in an unknown state.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 01 c8 37 a5 e0  Error: UNC 1 sectors at LBA = 0x00a537c8 = 10827720

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 01 c8 37 a5 e0 00      08:55:10.672  READ DMA
  c8 00 01 98 86 eb e3 00      08:55:10.672  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:10.656  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:09.648  READ DMA
  c8 00 01 88 7d cb e8 00      08:55:09.632  READ DMA

Error 12702 occurred at disk power-on lifetime: 2348 hours (97 days + 20 hours)
  When the command that caused the error occurred, the device was in an unknown state.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 01 c8 37 a5 e0  Error: UNC 1 sectors at LBA = 0x00a537c8 = 10827720

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 01 c8 37 a5 e0 00      08:55:09.648  READ DMA
  c8 00 01 88 7d cb e8 00      08:55:09.632  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:09.616  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:08.608  READ DMA
  c8 00 01 ac 8c fe e1 00      08:55:08.608  READ DMA

Error 12701 occurred at disk power-on lifetime: 2348 hours (97 days + 20 hours)
  When the command that caused the error occurred, the device was in an unknown state.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 01 c8 37 a5 e0  Error: UNC 1 sectors at LBA = 0x00a537c8 = 10827720

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 01 c8 37 a5 e0 00      08:55:08.608  READ DMA
  c8 00 01 ac 8c fe e1 00      08:55:08.608  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:08.592  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:07.584  READ DMA
  c8 00 01 f7 08 3a e4 00      08:55:07.568  READ DMA

Error 12700 occurred at disk power-on lifetime: 2348 hours (97 days + 20 hours)
  When the command that caused the error occurred, the device was in an unknown state.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 01 c8 37 a5 e0  Error: UNC 1 sectors at LBA = 0x00a537c8 = 10827720

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 01 c8 37 a5 e0 00      08:55:07.584  READ DMA
  c8 00 01 f7 08 3a e4 00      08:55:07.568  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:07.552  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:06.544  READ DMA
  c8 00 01 2b 41 40 e0 00      08:55:06.544  READ DMA

Error 12699 occurred at disk power-on lifetime: 2348 hours (97 days + 20 hours)
  When the command that caused the error occurred, the device was in an unknown state.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 01 c8 37 a5 e0  Error: UNC 1 sectors at LBA = 0x00a537c8 = 10827720

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 01 c8 37 a5 e0 00      08:55:06.544  READ DMA
  c8 00 01 2b 41 40 e0 00      08:55:06.544  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:06.512  READ DMA
  c8 00 01 c8 37 a5 e0 00      08:55:05.504  READ DMA
  c8 00 01 cf 64 d1 e2 00      08:55:05.504  READ DMA

SMART Extended Self-test Log (GP Log 0x07) not supported

SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed without error       00%         0         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Commands not supported

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11) not supported

