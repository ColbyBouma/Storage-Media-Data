smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.0-50-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     WDC WD1600ADFD-60NLR1
Serial Number:    WD-WMAP41632235
Firmware Version: 20.07P20
User Capacity:    160,041,885,696 bytes [160 GB]
Sector Size:      512 bytes logical/physical
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   ATA/ATAPI-7 published, ANSI INCITS 397-2005
Local Time is:    Tue May  4 12:04:05 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Unavailable
Write SCT (Get) Feature Control Command failed: scsi error badly formed scsi parameters
Wt Cache Reorder: Unknown (SCT Feature Control command failed)

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x82)	Offline data collection activity
					was completed without error.
					Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		( 5467) seconds.
Offline data collection
capabilities: 			 (0x7b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 (  81) minutes.
Conveyance self-test routine
recommended polling time: 	 (   5) minutes.
SCT capabilities: 	       (0x103f)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     PO-R--   200   200   051    -    0
  3 Spin_Up_Time            POS---   165   163   021    -    4783
  4 Start_Stop_Count        -O--CK   100   100   040    -    88
  5 Reallocated_Sector_Ct   PO--CK   200   200   140    -    0
  7 Seek_Error_Rate         -O-R--   200   200   051    -    0
  9 Power_On_Hours          -O--CK   050   050   000    -    36565
 10 Spin_Retry_Count        -O--C-   100   253   051    -    0
 11 Calibration_Retry_Count -O--C-   100   253   051    -    0
 12 Power_Cycle_Count       -O--CK   100   100   000    -    88
194 Temperature_Celsius     -O---K   121   091   000    -    26
196 Reallocated_Event_Count -O--CK   200   200   000    -    0
197 Current_Pending_Sector  -O--C-   200   200   000    -    0
198 Offline_Uncorrectable   -O--C-   200   200   000    -    0
199 UDMA_CRC_Error_Count    -O-R--   200   253   000    -    0
200 Multi_Zone_Error_Rate   ---R--   200   200   051    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      2  Comprehensive SMART error log
0x03       GPL     R/O      2  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      2  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xa0-0xa7  GPL,SL  VS      16  Device vendor specific log
0xa8-0xb7  GPL,SL  VS       1  Device vendor specific log
0xc0       GPL,SL  VS       1  Device vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (2 sectors)
No Errors Logged

SMART Extended Self-test Log Version: 1 (2 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed without error       00%         0         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  2
SCT Version (vendor specific):       258 (0x0102)
Device State:                        Active (0)
Current Temperature:                    26 Celsius
Power Cycle Min/Max Temperature:     --/26 Celsius
Lifetime    Min/Max Temperature:     --/56 Celsius

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      5/55 Celsius
Min/Max Temperature Limit:            1/60 Celsius
Temperature History Size (Index):    128 (27)

Index    Estimated Time   Temperature Celsius
  28    2021-05-04 09:57    30  ***********
 ...    ..(  8 skipped).    ..  ***********
  37    2021-05-04 10:06    30  ***********
  38    2021-05-04 10:07    31  ************
  39    2021-05-04 10:08    30  ***********
  40    2021-05-04 10:09    30  ***********
  41    2021-05-04 10:10    30  ***********
  42    2021-05-04 10:11    31  ************
 ...    ..( 29 skipped).    ..  ************
  72    2021-05-04 10:41    31  ************
  73    2021-05-04 10:42    30  ***********
  74    2021-05-04 10:43    31  ************
  75    2021-05-04 10:44    30  ***********
  76    2021-05-04 10:45    31  ************
  77    2021-05-04 10:46    31  ************
  78    2021-05-04 10:47    31  ************
  79    2021-05-04 10:48    30  ***********
  80    2021-05-04 10:49    31  ************
  81    2021-05-04 10:50    31  ************
  82    2021-05-04 10:51    30  ***********
  83    2021-05-04 10:52    31  ************
  84    2021-05-04 10:53    30  ***********
  85    2021-05-04 10:54    31  ************
  86    2021-05-04 10:55    30  ***********
  87    2021-05-04 10:56    31  ************
  88    2021-05-04 10:57    30  ***********
  89    2021-05-04 10:58    31  ************
  90    2021-05-04 10:59    30  ***********
  91    2021-05-04 11:00    30  ***********
  92    2021-05-04 11:01    31  ************
  93    2021-05-04 11:02    31  ************
  94    2021-05-04 11:03    31  ************
  95    2021-05-04 11:04    30  ***********
  96    2021-05-04 11:05    30  ***********
  97    2021-05-04 11:06    31  ************
  98    2021-05-04 11:07    30  ***********
  99    2021-05-04 11:08    31  ************
 100    2021-05-04 11:09    30  ***********
 101    2021-05-04 11:10    30  ***********
 102    2021-05-04 11:11    30  ***********
 103    2021-05-04 11:12     ?  -
 104    2021-05-04 11:13    23  ****
 105    2021-05-04 11:14    24  *****
 106    2021-05-04 11:15    25  ******
 107    2021-05-04 11:16    25  ******
 108    2021-05-04 11:17    26  *******
 109    2021-05-04 11:18    27  ********
 110    2021-05-04 11:19    28  *********
 111    2021-05-04 11:20    29  **********
 112    2021-05-04 11:21    29  **********
 113    2021-05-04 11:22    30  ***********
 114    2021-05-04 11:23    31  ************
 115    2021-05-04 11:24    32  *************
 116    2021-05-04 11:25    33  **************
 117    2021-05-04 11:26    33  **************
 118    2021-05-04 11:27    34  ***************
 119    2021-05-04 11:28    34  ***************
 120    2021-05-04 11:29    35  ****************
 121    2021-05-04 11:30    35  ****************
 122    2021-05-04 11:31    36  *****************
 ...    ..(  2 skipped).    ..  *****************
 125    2021-05-04 11:34    36  *****************
 126    2021-05-04 11:35    37  ******************
 127    2021-05-04 11:36    37  ******************
   0    2021-05-04 11:37    38  *******************
 ...    ..(  3 skipped).    ..  *******************
   4    2021-05-04 11:41    38  *******************
   5    2021-05-04 11:42    39  ********************
   6    2021-05-04 11:43    39  ********************
   7    2021-05-04 11:44    39  ********************
   8    2021-05-04 11:45     ?  -
   9    2021-05-04 11:46    26  *******
  10    2021-05-04 11:47    26  *******
  11    2021-05-04 11:48    27  ********
  12    2021-05-04 11:49    28  *********
  13    2021-05-04 11:50    29  **********
  14    2021-05-04 11:51    30  ***********
  15    2021-05-04 11:52    31  ************
  16    2021-05-04 11:53    31  ************
  17    2021-05-04 11:54    32  *************
  18    2021-05-04 11:55    32  *************
  19    2021-05-04 11:56    33  **************
  20    2021-05-04 11:57    33  **************
  21    2021-05-04 11:58    34  ***************
  22    2021-05-04 11:59    34  ***************
  23    2021-05-04 12:00    35  ****************
  24    2021-05-04 12:01    35  ****************
  25    2021-05-04 12:02     ?  -
  26    2021-05-04 12:03    26  *******
  27    2021-05-04 12:04    26  *******

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x8000  4           72  Vendor specific

