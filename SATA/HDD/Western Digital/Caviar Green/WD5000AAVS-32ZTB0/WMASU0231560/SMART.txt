smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.13.0-21-generic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Western Digital Caviar Green
Device Model:     WDC WD5000AAVS-32ZTB0
Serial Number:    WD-WMASU0231560
LU WWN Device Id: 5 0014ee 0aafcdd41
Firmware Version: 01.01B01
User Capacity:    500,107,862,016 bytes [500 GB]
Sector Size:      512 bytes logical/physical
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS (minor revision not indicated)
SATA Version is:  SATA 2.5, 3.0 Gb/s
Local Time is:    Mon Jan  3 18:12:15 2022 MST
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM level is:     254 (maximum performance), recommended: 128
APM feature is:   Unavailable
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x84)	Offline data collection activity
					was suspended by an interrupting command from host.
					Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(13200) seconds.
Offline data collection
capabilities: 			 (0x7b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 154) minutes.
Conveyance self-test routine
recommended polling time: 	 (   5) minutes.
SCT capabilities: 	       (0x303f)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR--   200   200   051    -    0
  3 Spin_Up_Time            PO----   169   155   021    -    4533
  4 Start_Stop_Count        -O--CK   094   094   000    -    6858
  5 Reallocated_Sector_Ct   PO--CK   200   200   140    -    0
  7 Seek_Error_Rate         -OSR--   100   253   051    -    0
  9 Power_On_Hours          -O--CK   062   062   000    -    28416
 10 Spin_Retry_Count        -O--C-   100   100   051    -    0
 11 Calibration_Retry_Count -O--C-   100   253   051    -    0
 12 Power_Cycle_Count       -O--CK   100   100   000    -    60
192 Power-Off_Retract_Count -O--CK   200   200   000    -    10
193 Load_Cycle_Count        -O--CK   190   190   000    -    31624
194 Temperature_Celsius     -O---K   125   094   000    -    22
196 Reallocated_Event_Count -O--CK   200   200   000    -    0
197 Current_Pending_Sector  -O--C-   200   200   000    -    0
198 Offline_Uncorrectable   ----C-   200   200   000    -    0
199 UDMA_CRC_Error_Count    -OSRCK   200   200   000    -    0
200 Multi_Zone_Error_Rate   ---R--   200   200   051    -    1
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      5  Comprehensive SMART error log
0x03       GPL     R/O      6  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xa0-0xa7  GPL,SL  VS      16  Device vendor specific log
0xa8-0xb7  GPL,SL  VS       1  Device vendor specific log
0xc0       GPL,SL  VS       1  Device vendor specific log
0xc1       GPL     VS      24  Device vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (6 sectors)
No Errors Logged

SMART Extended Self-test Log Version: 1 (1 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  2
SCT Version (vendor specific):       258 (0x0102)
Device State:                        SMART Off-line Data Collection executing in background (4)
Current Temperature:                    22 Celsius
Power Cycle Min/Max Temperature:     22/22 Celsius
Lifetime    Min/Max Temperature:     22/53 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      0/60 Celsius
Min/Max Temperature Limit:           -40/85 Celsius
Temperature History Size (Index):    128 (87)

Index    Estimated Time   Temperature Celsius
  88    2022-01-03 16:05    35  ****************
 ...    ..(  3 skipped).    ..  ****************
  92    2022-01-03 16:09    35  ****************
  93    2022-01-03 16:10    36  *****************
 ...    ..(  3 skipped).    ..  *****************
  97    2022-01-03 16:14    36  *****************
  98    2022-01-03 16:15    35  ****************
 ...    ..(  6 skipped).    ..  ****************
 105    2022-01-03 16:22    35  ****************
 106    2022-01-03 16:23    36  *****************
 107    2022-01-03 16:24     ?  -
 108    2022-01-03 16:25    33  **************
 109    2022-01-03 16:26    34  ***************
 ...    ..(  8 skipped).    ..  ***************
 118    2022-01-03 16:35    34  ***************
 119    2022-01-03 16:36    33  **************
 ...    ..(  6 skipped).    ..  **************
 126    2022-01-03 16:43    33  **************
 127    2022-01-03 16:44    34  ***************
 ...    ..(  4 skipped).    ..  ***************
   4    2022-01-03 16:49    34  ***************
   5    2022-01-03 16:50    35  ****************
   6    2022-01-03 16:51    35  ****************
   7    2022-01-03 16:52    35  ****************
   8    2022-01-03 16:53    36  *****************
 ...    ..(  2 skipped).    ..  *****************
  11    2022-01-03 16:56    36  *****************
  12    2022-01-03 16:57    37  ******************
 ...    ..(  3 skipped).    ..  ******************
  16    2022-01-03 17:01    37  ******************
  17    2022-01-03 17:02    38  *******************
 ...    ..(  3 skipped).    ..  *******************
  21    2022-01-03 17:06    38  *******************
  22    2022-01-03 17:07    39  ********************
 ...    ..(  8 skipped).    ..  ********************
  31    2022-01-03 17:16    39  ********************
  32    2022-01-03 17:17     ?  -
  33    2022-01-03 17:18    40  *********************
  34    2022-01-03 17:19     ?  -
  35    2022-01-03 17:20    39  ********************
  36    2022-01-03 17:21     ?  -
  37    2022-01-03 17:22    38  *******************
 ...    ..(  5 skipped).    ..  *******************
  43    2022-01-03 17:28    38  *******************
  44    2022-01-03 17:29    37  ******************
 ...    ..(  5 skipped).    ..  ******************
  50    2022-01-03 17:35    37  ******************
  51    2022-01-03 17:36    36  *****************
 ...    ..( 15 skipped).    ..  *****************
  67    2022-01-03 17:52    36  *****************
  68    2022-01-03 17:53    37  ******************
 ...    ..(  3 skipped).    ..  ******************
  72    2022-01-03 17:57    37  ******************
  73    2022-01-03 17:58    38  *******************
 ...    ..(  3 skipped).    ..  *******************
  77    2022-01-03 18:02    38  *******************
  78    2022-01-03 18:03    39  ********************
  79    2022-01-03 18:04    39  ********************
  80    2022-01-03 18:05     ?  -
  81    2022-01-03 18:06    21  **
  82    2022-01-03 18:07     ?  -
  83    2022-01-03 18:08    21  **
  84    2022-01-03 18:09     ?  -
  85    2022-01-03 18:10    22  ***
  86    2022-01-03 18:11    22  ***
  87    2022-01-03 18:12    35  ****************

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x8000  4           79  Vendor specific

