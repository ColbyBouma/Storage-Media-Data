smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.0-50-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     WL6000GSA6457
Serial Number:    WOL240336031
LU WWN Device Id: 5 0014ee 059384e4c
Firmware Version: 82.00A82
User Capacity:    6,000,527,425,536 bytes [6.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5700 rpm
Form Factor:      3.5 inches
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   ACS-2, ACS-3 T13/2161-D revision 3b
SATA Version is:  SATA 3.1, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Tue May  4 11:35:33 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x84)	Offline data collection activity
					was suspended by an interrupting command from host.
					Auto Offline Data Collection: Enabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		( 5564) seconds.
Offline data collection
capabilities: 			 (0x7b) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 709) minutes.
Conveyance self-test routine
recommended polling time: 	 (   5) minutes.
SCT capabilities: 	       (0x303d)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   194   170   051    -    473
  3 Spin_Up_Time            PO--CK   197   197   021    -    9150
  4 Start_Stop_Count        -O--CK   100   100   000    -    18
  5 Reallocated_Sector_Ct   POSR-K   200   200   140    -    0
  7 Seek_Error_Rate         -O--CK   100   253   051    -    0
  9 Power_On_Hours          -O--CK   059   059   000    -    30239
 10 Spin_Retry_Count        -O--CK   100   253   051    -    0
 11 Calibration_Retry_Count -O--CK   100   253   051    -    0
 12 Power_Cycle_Count       -O--CK   100   100   000    -    18
192 Power-Off_Retract_Count -O--CK   200   200   000    -    15
193 Load_Cycle_Count        ----CK   200   200   000    -    234
194 Temperature_Celsius     -O--CK   126   109   000    -    26
196 Reallocated_Event_Count -O--CK   200   200   000    -    0
197 Current_Pending_Sector  -O--CK   200   200   000    -    97
198 Offline_Uncorrectable   -O--CK   200   200   000    -    62
199 UDMA_CRC_Error_Count    -O--CK   200   200   000    -    0
200 Multi_Zone_Error_Rate   ---R--   200   181   051    -    19
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      5  Comprehensive SMART error log
0x03       GPL     R/O      6  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x21       GPL     R/O      1  Write stream error log
0x22       GPL     R/O      1  Read stream error log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xa0-0xa7  GPL,SL  VS      16  Device vendor specific log
0xa8-0xb6  GPL,SL  VS       1  Device vendor specific log
0xb7       GPL,SL  VS      54  Device vendor specific log
0xbd       GPL,SL  VS       1  Device vendor specific log
0xc0       GPL,SL  VS       1  Device vendor specific log
0xc1       GPL     VS      93  Device vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (6 sectors)
Device Error Count: 27 (device log contains only the most recent 24 errors)
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 27 [2] occurred at disk power-on lifetime: 30231 hours (1259 days + 15 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 f1 9e 0e 68 40 00  Error: UNC at LBA = 0xf19e0e68 = 4053667432

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 e8 00 08 00 00 f1 9e 0d e0 40 00  3d+12:33:44.814  READ FPDMA QUEUED
  60 00 e0 00 00 00 00 f1 9e 0d 00 40 00  3d+12:33:44.814  READ FPDMA QUEUED
  60 01 00 00 00 00 00 f1 9e 0c 00 40 00  3d+12:33:44.812  READ FPDMA QUEUED
  60 01 00 00 08 00 00 f1 9e 0b 00 40 00  3d+12:33:44.811  READ FPDMA QUEUED
  60 00 e0 00 00 00 00 f1 9e 0a 20 40 00  3d+12:33:44.811  READ FPDMA QUEUED

Error 26 [1] occurred at disk power-on lifetime: 30227 hours (1259 days + 11 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 01 32 41 26 a0 40 00  Error: UNC at LBA = 0x1324126a0 = 5138097824

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 10 00 20 00 02 ba 8d a4 90 40 00  3d+08:01:18.345  READ FPDMA QUEUED
  60 00 10 00 18 00 02 ba 8d a2 90 40 00  3d+08:01:18.345  READ FPDMA QUEUED
  60 00 10 00 10 00 00 00 40 02 90 40 00  3d+08:01:18.345  READ FPDMA QUEUED
  60 00 10 00 08 00 00 b1 0f 3a e8 40 00  3d+08:01:18.345  READ FPDMA QUEUED
  60 00 38 00 00 00 01 32 41 26 80 40 00  3d+08:01:18.345  READ FPDMA QUEUED

Error 25 [0] occurred at disk power-on lifetime: 30227 hours (1259 days + 11 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 01 32 41 26 30 40 00  Error: UNC at LBA = 0x132412630 = 5138097712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 08 00 10 00 01 32 41 28 f8 40 00  3d+08:01:03.611  READ FPDMA QUEUED
  60 00 38 00 00 00 01 32 41 26 80 40 00  3d+08:01:03.611  READ FPDMA QUEUED
  60 01 00 00 08 00 01 32 41 25 78 40 00  3d+08:01:03.611  READ FPDMA QUEUED
  60 00 08 00 08 00 01 32 41 25 80 40 00  3d+08:01:03.570  READ FPDMA QUEUED
  60 00 08 00 00 00 01 32 41 25 88 40 00  3d+08:01:03.570  READ FPDMA QUEUED

Error 24 [23] occurred at disk power-on lifetime: 30227 hours (1259 days + 11 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 01 30 fe 5f 88 40 00  Error: UNC at LBA = 0x130fe5f88 = 5116944264

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 30 00 08 00 01 30 fe 5f 58 40 00  3d+07:58:44.038  READ FPDMA QUEUED
  60 00 30 00 00 00 01 30 fe 5f 88 40 00  3d+07:58:44.034  READ FPDMA QUEUED
  60 00 28 00 00 00 01 30 fe 5f 30 40 00  3d+07:58:44.030  READ FPDMA QUEUED
  60 00 30 00 00 00 01 30 fe 5f 00 40 00  3d+07:58:44.026  READ FPDMA QUEUED
  60 00 28 00 00 00 01 30 fe 5e d8 40 00  3d+07:58:44.022  READ FPDMA QUEUED

Error 23 [22] occurred at disk power-on lifetime: 30081 hours (1253 days + 9 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 01 2e ae 3d d8 40 00  Error: UNC at LBA = 0x12eae3dd8 = 5078138328

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 b8 00 00 00 01 2e ae 3d e0 40 00 29d+22:41:58.033  READ FPDMA QUEUED
  60 00 28 00 08 00 01 2e ae 3d b8 40 00 29d+22:41:58.033  READ FPDMA QUEUED
  60 00 30 00 00 00 01 2e ae 3d 88 40 00 29d+22:41:58.033  READ FPDMA QUEUED
  60 00 88 00 00 00 01 2e ae 3d 00 40 00 29d+22:41:58.033  READ FPDMA QUEUED
  60 00 28 00 08 00 01 2e ae 3c d8 40 00 29d+22:41:58.033  READ FPDMA QUEUED

Error 22 [21] occurred at disk power-on lifetime: 30081 hours (1253 days + 9 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 01 32 41 26 c0 40 00  Error: UNC at LBA = 0x1324126c0 = 5138097856

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 08 00 00 00 01 32 43 56 10 40 00 29d+22:31:59.882  READ FPDMA QUEUED
  60 00 10 00 00 00 01 32 43 03 10 40 00 29d+22:31:59.882  READ FPDMA QUEUED
  60 00 88 00 08 00 01 32 41 26 a8 40 00 29d+22:31:59.877  READ FPDMA QUEUED
  60 00 10 00 00 00 01 32 40 e0 70 40 00 29d+22:31:59.865  READ FPDMA QUEUED
  60 00 20 00 08 00 01 32 40 da 18 40 00 29d+22:31:59.857  READ FPDMA QUEUED

Error 21 [20] occurred at disk power-on lifetime: 30081 hours (1253 days + 9 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 01 32 41 26 30 40 00  Error: UNC at LBA = 0x132412630 = 5138097712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 08 00 00 00 01 32 41 26 88 40 00 29d+22:31:50.793  READ FPDMA QUEUED
  60 00 d8 00 08 00 01 32 41 25 c0 40 00 29d+22:31:50.783  READ FPDMA QUEUED
  60 00 08 00 00 00 01 32 41 25 c8 40 00 29d+22:31:47.875  READ FPDMA QUEUED
  60 00 38 00 08 00 01 32 41 25 78 40 00 29d+22:31:47.875  READ FPDMA QUEUED
  60 00 20 00 00 00 01 32 41 24 48 40 00 29d+22:31:47.875  READ FPDMA QUEUED

Error 20 [19] occurred at disk power-on lifetime: 29913 hours (1246 days + 9 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 f2 b3 94 e8 40 00  Error: UNC at LBA = 0xf2b394e8 = 4071855336

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  60 00 e0 00 08 00 00 f2 b3 95 68 40 00 22d+22:48:39.119  READ FPDMA QUEUED
  60 00 f0 00 00 00 00 f2 b3 94 78 40 00 22d+22:48:39.119  READ FPDMA QUEUED
  60 00 e0 00 08 00 00 f2 b3 93 98 40 00 22d+22:48:37.037  READ FPDMA QUEUED
  60 00 e8 00 00 00 00 f2 b3 92 b0 40 00 22d+22:48:36.006  READ FPDMA QUEUED
  60 00 e0 00 08 00 00 f2 b3 91 d0 40 00 22d+22:48:36.006  READ FPDMA QUEUED

SMART Extended Self-test Log Version: 1 (1 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Extended offline    Completed: read failure       90%     30201         19776
# 2  Extended offline    Completed: read failure       90%     30052         56872
# 3  Extended offline    Completed: read failure       80%     29887         2205713568
# 4  Extended offline    Completed: read failure       90%     29717         2971668760
# 5  Extended offline    Completed: read failure       90%     29549         60550
# 6  Extended offline    Completed: read failure       90%     29381         60544
# 7  Extended offline    Completed: read failure       90%     29213         60544
# 8  Extended offline    Completed: read failure       90%     29045         71670
# 9  Extended offline    Completed: read failure       90%     28877         71664
#10  Extended offline    Completed: read failure       90%     28710         172168
#11  Extended offline    Completed: read failure       60%     28545         4057164512
#12  Extended offline    Completed: read failure       60%     28377         4060645104
#13  Extended offline    Completed: read failure       70%     28210         4067947344
#14  Extended offline    Completed without error       00%     28051         -
#15  Extended offline    Completed without error       00%     27883         -
#16  Extended offline    Completed: read failure       70%     27706         4057138024
#17  Extended offline    Completed without error       00%     27548         -
#18  Extended offline    Completed without error       00%     27380         -
1 of 14 failed self-tests are outdated by newer successful extended offline self-test #14

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       258 (0x0102)
Device State:                        Active (0)
Current Temperature:                    26 Celsius
Power Cycle Min/Max Temperature:     26/26 Celsius
Lifetime    Min/Max Temperature:     17/43 Celsius
Under/Over Temperature Limit Count:   0/0
Vendor specific:
01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      0/60 Celsius
Min/Max Temperature Limit:           -41/85 Celsius
Temperature History Size (Index):    478 (473)

Index    Estimated Time   Temperature Celsius
 474    2021-05-04 03:38    37  ******************
 ...    ..(382 skipped).    ..  ******************
 379    2021-05-04 10:01    37  ******************
 380    2021-05-04 10:02     ?  -
 381    2021-05-04 10:03    25  ******
 382    2021-05-04 10:04    26  *******
 383    2021-05-04 10:05    26  *******
 384    2021-05-04 10:06    26  *******
 385    2021-05-04 10:07    27  ********
 386    2021-05-04 10:08    27  ********
 387    2021-05-04 10:09    28  *********
 388    2021-05-04 10:10     ?  -
 389    2021-05-04 10:11    26  *******
 390    2021-05-04 10:12    39  ********************
 ...    ..(  6 skipped).    ..  ********************
 397    2021-05-04 10:19    39  ********************
 398    2021-05-04 10:20    38  *******************
 ...    ..( 37 skipped).    ..  *******************
 436    2021-05-04 10:58    38  *******************
 437    2021-05-04 10:59    37  ******************
 ...    ..( 35 skipped).    ..  ******************
 473    2021-05-04 11:35    37  ******************

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0008  2            0  Device-to-host non-data FIS retries
0x0009  2            0  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS
0x000f  2            0  R_ERR response for host-to-device data FIS, CRC
0x0012  2            0  R_ERR response for host-to-device non-data FIS, CRC
0x8000  4           37  Vendor specific

