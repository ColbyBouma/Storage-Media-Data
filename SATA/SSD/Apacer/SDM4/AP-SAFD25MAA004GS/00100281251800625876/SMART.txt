smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.13.0-22-generic] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Apacer SDM4 Series SSD Module
Device Model:     4GB SATA Flash Drive
Serial Number:    00100281251800625876
Firmware Version: SFI2101D
User Capacity:    3,952,263,168 bytes [3.95 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
TRIM Command:     Available
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ATA8-ACS, ATA/ATAPI-4 published, ANSI NCITS 317-1998
SATA Version is:  SATA 2.6, 3.0 Gb/s
Local Time is:    Mon Jan 10 17:46:20 2022 MST
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Unavailable

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    1) seconds.
Offline data collection
capabilities: 			 (0x75) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Abort Offline collection upon new
					command.
					No Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					No General Purpose Logging support.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (   1) minutes.
Conveyance self-test routine
recommended polling time: 	 (   1) minutes.

SMART Attributes Data Structure revision number: 7
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
 12 Power_Cycle_Count       ------+  100   100   000    -    2527
160 Initial_Bad_Block_Count ------+  100   100   000    -    11
161 Bad_Block_Count         ------+  100   100   000    -    11
162 Spare_Block_Count       ------+  100   100   000    -    309
163 Max_Erase_Count         ------+  100   100   000    -    1998
164 Average_Erase_Count     ------+  100   100   000    -    1607
165 Average_Erase_Count     ------+  100   100   000    -    1607
241 Total_LBAs_Written      ------+  100   100   000    -    1319883242
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory not supported

SMART Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00           SL  R/O      1  Log Directory
0x80-0x9f      SL  R/W     16  Host vendor specific log
0xa0-0xbf      SL  VS      16  Device vendor specific log

SMART Extended Comprehensive Error Log (GP Log 0x03) not supported

Read SMART Error Log failed: scsi error badly formed scsi parameters

SMART Extended Self-test Log (GP Log 0x07) not supported

Read SMART Self-test Log failed: scsi error badly formed scsi parameters

Read SMART Selective Self-test Log failed: scsi error badly formed scsi parameters

SCT Commands not supported

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11) not supported

