
  +--------------------------------------------------------------------------+
  | ReadSpeed: Hyper-accurate mass storage read-performance benchmark. rel 1 |
  |  Benchmarked values are in megabytes read per second at five locations.  |
  +--------------------------------------------------------------------------+

Driv Size  Drive Identity     Location:    0      25%     50%     75%     100
---- ----- ---------------------------- ------- ------- ------- ------- -------
 81  120GB CT120BX100SSD1                520.3   542.5   420.4   431.2   542.6 

                  Benchmarked: Sunday, 2021-01-03 at 06:02
 -----------------------------------------------------------------------------
   See the ReadSpeed forums at forums.grc.com for help and community support.  

