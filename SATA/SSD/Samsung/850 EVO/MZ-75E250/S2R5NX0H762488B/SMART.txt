smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.0-50-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Samsung based SSDs
Device Model:     Samsung SSD 850 EVO 250GB
Serial Number:    S2R5NX0H762488B
LU WWN Device Id: 5 002538 d411e87af
Firmware Version: EMT02B6Q
User Capacity:    250,059,350,016 bytes [250 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ACS-2, ATA8-ACS T13/1699-D revision 4c
SATA Version is:  SATA 3.1, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Mon May  3 01:06:53 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    0) seconds.
Offline data collection
capabilities: 			 (0x53) SMART execute Offline immediate.
					Auto Offline data collection on/off support.
					Suspend Offline collection upon new
					command.
					No Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   2) minutes.
Extended self-test routine
recommended polling time: 	 ( 133) minutes.
SCT capabilities: 	       (0x003d)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 1
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  5 Reallocated_Sector_Ct   PO--CK   100   100   010    -    0
  9 Power_On_Hours          -O--CK   098   098   000    -    6566
 12 Power_Cycle_Count       -O--CK   099   099   000    -    109
177 Wear_Leveling_Count     PO--C-   099   099   000    -    2
179 Used_Rsvd_Blk_Cnt_Tot   PO--C-   100   100   010    -    0
181 Program_Fail_Cnt_Total  -O--CK   100   100   010    -    0
182 Erase_Fail_Count_Total  -O--CK   100   100   010    -    0
183 Runtime_Bad_Block       PO--C-   100   100   010    -    0
187 Uncorrectable_Error_Cnt -O--CK   100   100   000    -    0
190 Airflow_Temperature_Cel -O--CK   068   062   000    -    32
195 ECC_Error_Rate          -O-RC-   200   200   000    -    0
199 CRC_Error_Count         -OSRCK   100   100   000    -    0
235 POR_Recovery_Count      -O--C-   099   099   000    -    103
241 Total_LBAs_Written      -O--CK   099   099   000    -    1942780600
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      1  Comprehensive SMART error log
0x03       GPL     R/O      1  Ext. Comprehensive SMART error log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x13       GPL     R/O      1  SATA NCQ Send and Receive log
0x30       GPL,SL  R/O      9  IDENTIFY DEVICE data log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xa1           SL  VS      16  Device vendor specific log
0xa5           SL  VS      16  Device vendor specific log
0xce           SL  VS      16  Device vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (1 sectors)
No Errors Logged

SMART Extended Self-test Log Version: 1 (1 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
  255        0    65535  Read_scanning was never started
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       256 (0x0100)
Device State:                        Active (0)
Current Temperature:                     ? Celsius
Power Cycle Min/Max Temperature:     28/28 Celsius
Lifetime    Min/Max Temperature:     26/40 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        10 minutes
Min/Max recommended Temperature:      0/70 Celsius
Min/Max Temperature Limit:            0/70 Celsius
Temperature History Size (Index):    128 (91)

Index    Estimated Time   Temperature Celsius
  92    2021-05-02 03:50    31  ************
 ...    ..(  3 skipped).    ..  ************
  96    2021-05-02 04:30    31  ************
  97    2021-05-02 04:40    32  *************
  98    2021-05-02 04:50    32  *************
  99    2021-05-02 05:00    32  *************
 100    2021-05-02 05:10    31  ************
 101    2021-05-02 05:20    32  *************
 102    2021-05-02 05:30    32  *************
 103    2021-05-02 05:40    34  ***************
 104    2021-05-02 05:50    33  **************
 105    2021-05-02 06:00    34  ***************
 106    2021-05-02 06:10    34  ***************
 107    2021-05-02 06:20    33  **************
 108    2021-05-02 06:30    32  *************
 109    2021-05-02 06:40    32  *************
 110    2021-05-02 06:50    33  **************
 111    2021-05-02 07:00    31  ************
 112    2021-05-02 07:10    31  ************
 113    2021-05-02 07:20    32  *************
 114    2021-05-02 07:30    32  *************
 115    2021-05-02 07:40    33  **************
 116    2021-05-02 07:50    32  *************
 117    2021-05-02 08:00    32  *************
 118    2021-05-02 08:10    33  **************
 119    2021-05-02 08:20    32  *************
 ...    ..(  2 skipped).    ..  *************
 122    2021-05-02 08:50    32  *************
 123    2021-05-02 09:00    31  ************
 ...    ..(  9 skipped).    ..  ************
   5    2021-05-02 10:40    31  ************
   6    2021-05-02 10:50    32  *************
   7    2021-05-02 11:00    31  ************
 ...    ..(  3 skipped).    ..  ************
  11    2021-05-02 11:40    31  ************
  12    2021-05-02 11:50    32  *************
  13    2021-05-02 12:00    31  ************
 ...    ..( 18 skipped).    ..  ************
  32    2021-05-02 15:10    31  ************
  33    2021-05-02 15:20    30  ***********
  34    2021-05-02 15:30    31  ************
 ...    ..(  3 skipped).    ..  ************
  38    2021-05-02 16:10    31  ************
  39    2021-05-02 16:20    30  ***********
  40    2021-05-02 16:30    30  ***********
  41    2021-05-02 16:40    32  *************
  42    2021-05-02 16:50    33  **************
  43    2021-05-02 17:00    32  *************
  44    2021-05-02 17:10    32  *************
  45    2021-05-02 17:20    30  ***********
  46    2021-05-02 17:30    30  ***********
  47    2021-05-02 17:40    29  **********
 ...    ..(  2 skipped).    ..  **********
  50    2021-05-02 18:10    29  **********
  51    2021-05-02 18:20    28  *********
  52    2021-05-02 18:30    28  *********
  53    2021-05-02 18:40    28  *********
  54    2021-05-02 18:50    29  **********
  55    2021-05-02 19:00    28  *********
  56    2021-05-02 19:10    29  **********
  57    2021-05-02 19:20    29  **********
  58    2021-05-02 19:30    28  *********
  59    2021-05-02 19:40    29  **********
  60    2021-05-02 19:50    32  *************
  61    2021-05-02 20:00    29  **********
  62    2021-05-02 20:10    31  ************
  63    2021-05-02 20:20    30  ***********
  64    2021-05-02 20:30    32  *************
  65    2021-05-02 20:40    31  ************
  66    2021-05-02 20:50    32  *************
  67    2021-05-02 21:00    32  *************
  68    2021-05-02 21:10    33  **************
  69    2021-05-02 21:20    34  ***************
 ...    ..(  2 skipped).    ..  ***************
  72    2021-05-02 21:50    34  ***************
  73    2021-05-02 22:00    31  ************
  74    2021-05-02 22:10    32  *************
  75    2021-05-02 22:20    33  **************
  76    2021-05-02 22:30    33  **************
  77    2021-05-02 22:40    33  **************
  78    2021-05-02 22:50    32  *************
  79    2021-05-02 23:00    31  ************
  80    2021-05-02 23:10    31  ************
  81    2021-05-02 23:20    30  ***********
  82    2021-05-02 23:30    32  *************
  83    2021-05-02 23:40    29  **********
  84    2021-05-02 23:50    27  ********
  85    2021-05-03 00:00    28  *********
  86    2021-05-03 00:10     ?  -
 ...    ..(  2 skipped).    ..  -
  89    2021-05-03 00:40     ?  -
  90    2021-05-03 00:50    28  *********
  91    2021-05-03 01:00     ?  -

SCT Error Recovery Control:
           Read: Disabled
          Write: Disabled

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0008  2            0  Device-to-host non-data FIS retries
0x0009  2            1  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            0  Device-to-host register FISes sent due to a COMRESET
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS
0x000f  2            0  R_ERR response for host-to-device data FIS, CRC
0x0010  2            0  R_ERR response for host-to-device data FIS, non-CRC
0x0012  2            0  R_ERR response for host-to-device non-data FIS, CRC
0x0013  2            0  R_ERR response for host-to-device non-data FIS, non-CRC

