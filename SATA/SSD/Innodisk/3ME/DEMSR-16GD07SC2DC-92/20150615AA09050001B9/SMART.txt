smartctl 7.3 2022-02-28 r5338 [x86_64-linux-5.19.0-23-generic] (local build)
Copyright (C) 2002-22, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Innodisk 1ME3/3ME/3SE SSDs
Device Model:     InnoDisk Corp. - mSATA 3ME
Serial Number:    20150615AA09050001B9
Firmware Version: S130710D
User Capacity:    16,013,942,784 bytes [16.0 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
TRIM Command:     Unavailable
Device is:        In smartctl database 7.3/5417
ATA Version is:   ATA8-ACS (minor revision not indicated)
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Sun Nov 27 17:45:43 2022 MST
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Enabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Read SCT Status failed: scsi error badly formed scsi parameters
Wt Cache Reorder: Unknown (SCT Feature Control command failed)

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Total time to complete Offline 
data collection: 		(   32) seconds.
Offline data collection
capabilities: 			 (0x00) 	Offline data collection not supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x00)	Error logging NOT supported.
					General Purpose Logging supported.
SCT capabilities: 	       (0x0039)	SCT Status supported.
					SCT Error Recovery Control supported.
					SCT Feature Control supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     ------   000   000   000    -    0
  2 Throughput_Performance  ------   000   000   000    -    0
  3 Spin_Up_Time            ------   000   000   000    -    0
  5 Reallocated_Sector_Ct   ------   000   000   000    -    0
  7 Seek_Error_Rate         ------   000   000   000    -    0
  8 Seek_Time_Performance   ------   000   000   000    -    0
  9 Power_On_Hours          ------   000   000   000    -    184
 10 Spin_Retry_Count        ------   000   000   000    -    0
 12 Power_Cycle_Count       ------   000   000   000    -    265
  1 Raw_Read_Error_Rate     ------   000   000   000    -    0
168 SATA_PHY_Error_Count    ------   000   000   000    -    0
169 Unknown_Innodisk_Attr   ------   000   000   000    -    0x000000000000
  1 Raw_Read_Error_Rate     ------   000   000   000    -    0
175 Bad_Cluster_Table_Count ------   100   000   000    -    0
192 Power-Off_Retract_Count ------   000   000   000    -    0
  1 Raw_Read_Error_Rate     ------   000   000   000    -    2199023255552
197 Current_Pending_Sector  ------   000   000   000    -    0
240 Write_Head              ------   000   000   000    -    0
170 Bad_Block_Count         PO----   100   100   ---    -    0 17 0
173 Erase_Count             -O--C-   100   100   ---    -    0 32 13
229 Flash_ID                -O----   100   100   ---    -    0x040408769498
236 Unstable_Power_Count    -O----   100   100   ---    -    0
235 Later_Bad_Block         -O----   100   000   ---    -    0
176 Uncorr_RECORD_Count     ------   100   000   ---    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

Read SMART Log Directory failed: scsi error badly formed scsi parameters

ATA_READ_LOG_EXT (addr=0x00:0x00, page=0, n=1) failed: scsi error badly formed scsi parameters
Read GP Log Directory failed

SMART Extended Comprehensive Error Log (GP Log 0x03) not supported

SMART Error Log not supported

SMART Extended Self-test Log (GP Log 0x07) not supported

SMART Self-test Log not supported

Selective Self-tests/Logging not supported

Read SCT Status failed: scsi error badly formed scsi parameters

Read SCT Status failed: scsi error badly formed scsi parameters
SCT (Get) Error Recovery Control command failed

Device Statistics (GP/SMART Log 0x04) not supported

Pending Defects log (GP Log 0x0c) not supported

ATA_READ_LOG_EXT (addr=0x11:0x00, page=0, n=1) failed: scsi error badly formed scsi parameters
Read SATA Phy Event Counters failed

