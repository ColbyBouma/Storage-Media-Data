smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.0-50-generic] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     SSD7EP7011-080-IGT-2
Serial Number:    PNY49170001356450873
LU WWN Device Id: 5 f8db4c 497450873
Firmware Version: EP701104
User Capacity:    80,026,361,856 bytes [80.0 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   ATA8-ACS, ACS-2 T13/2015-D revision 3
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Mon May  3 01:00:21 2021 MDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM level is:     254 (maximum performance)
Rd look-ahead is: Disabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, NOT FROZEN [SEC1]
Wt Cache Reorder: Unavailable

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    0) seconds.
Offline data collection
capabilities: 			 (0x7d) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Abort Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0003)	Saves SMART data before entering
					power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (  48) minutes.
Conveyance self-test routine
recommended polling time: 	 (   2) minutes.
SCT capabilities: 	       (0x0025)	SCT Status supported.
					SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     PO--CK   120   120   050    -    4294967296
  5 Reallocated_Sector_Ct   PO--CK   100   100   003    -    0
  9 Power_On_Hours          -O--CK   090   090   000    -    9172 (65 24 0)
 12 Power_Cycle_Count       -O--CK   100   100   000    -    64
 13 Read_Soft_Error_Rate    -O--CK   120   120   000    -    0
100 Unknown_Attribute       -O--CK   000   000   000    -    858
170 Unknown_Attribute       -O--CK   000   000   000    -    3936
171 Unknown_Attribute       -O-R--   100   100   000    -    0
172 Unknown_Attribute       -O--CK   100   100   000    -    0
174 Unknown_Attribute       ----CK   000   000   000    -    64
175 Program_Fail_Count_Chip ------   029   048   000    -    73017589789
177 Wear_Leveling_Count     ------   000   000   000    -    1
181 Program_Fail_Cnt_Total  -O-R--   100   100   000    -    0
182 Erase_Fail_Count_Total  -O--CK   100   100   000    -    0
184 End-to-End_Error        -O--CK   100   100   090    -    0
187 Reported_Uncorrect      -O--C-   100   100   000    -    0
194 Temperature_Celsius     -O---K   029   048   000    -    29 (Min/Max 17/48)
195 Hardware_ECC_Recovered  --SRC-   120   120   000    -    4294967296
196 Reallocated_Event_Count PO--CK   100   100   003    -    0
199 UDMA_CRC_Error_Count    -O--CK   200   200   000    -    0
201 Unknown_SSD_Attribute   --SRC-   120   120   000    -    4294967296
204 Soft_ECC_Correction     --SRC-   120   120   000    -    4294967296
230 Unknown_SSD_Attribute   PO--C-   100   100   000    -    0
231 Unknown_SSD_Attribute   PO--C-   100   100   010    -    223338299392
232 Available_Reservd_Space -O--CK   000   000   000    -    15
233 Media_Wearout_Indicator -O--CK   000   000   000    -    849
234 Unknown_Attribute       -O--CK   000   000   000    -    4641
235 Unknown_Attribute       PO--CK   100   100   002    -    0
241 Total_LBAs_Written      -O--CK   000   000   000    -    4641
242 Total_LBAs_Read         -O--CK   000   000   000    -    422
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x03       GPL     R/O     16  Ext. Comprehensive SMART error log
0x04       GPL,SL  R/O      6  Device Statistics log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xa8       GPL,SL  VS      16  Device vendor specific log
0xb7       GPL,SL  VS      16  Device vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (16 sectors)
No Errors Logged

SMART Extended Self-test Log Version: 1 (1 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       0 (0x0000)
Device State:                        Active (0)
Current Temperature:                    29 Celsius
Power Cycle Min/Max Temperature:     17/48 Celsius
Lifetime    Min/Max Temperature:     17/48 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        10 minutes
Min/Max recommended Temperature:      0/65 Celsius
Min/Max Temperature Limit:            0/70 Celsius
Temperature History Size (Index):    478 (292)

Index    Estimated Time   Temperature Celsius
 293    2021-04-29 17:30    35  ****************
 294    2021-04-29 17:40    36  *****************
 ...    ..( 11 skipped).    ..  *****************
 306    2021-04-29 19:40    36  *****************
 307    2021-04-29 19:50     ?  -
 308    2021-04-29 20:00    36  *****************
 ...    ..( 56 skipped).    ..  *****************
 365    2021-04-30 05:30    36  *****************
 366    2021-04-30 05:40    35  ****************
 ...    ..( 10 skipped).    ..  ****************
 377    2021-04-30 07:30    35  ****************
 378    2021-04-30 07:40    36  *****************
 379    2021-04-30 07:50    35  ****************
 ...    ..( 60 skipped).    ..  ****************
 440    2021-04-30 18:00    35  ****************
 441    2021-04-30 18:10    36  *****************
 ...    ..(105 skipped).    ..  *****************
  69    2021-05-01 11:50    36  *****************
  70    2021-05-01 12:00    35  ****************
 ...    ..( 18 skipped).    ..  ****************
  89    2021-05-01 15:10    35  ****************
  90    2021-05-01 15:20    36  *****************
  91    2021-05-01 15:30    36  *****************
  92    2021-05-01 15:40    35  ****************
  93    2021-05-01 15:50    35  ****************
  94    2021-05-01 16:00    36  *****************
 ...    ..( 48 skipped).    ..  *****************
 143    2021-05-02 00:10    36  *****************
 144    2021-05-02 00:20    37  ******************
 145    2021-05-02 00:30    36  *****************
 146    2021-05-02 00:40    37  ******************
 147    2021-05-02 00:50    36  *****************
 ...    ..(  2 skipped).    ..  *****************
 150    2021-05-02 01:20    36  *****************
 151    2021-05-02 01:30    37  ******************
 152    2021-05-02 01:40    36  *****************
 ...    ..( 60 skipped).    ..  *****************
 213    2021-05-02 11:50    36  *****************
 214    2021-05-02 12:00    35  ****************
 ...    ..( 19 skipped).    ..  ****************
 234    2021-05-02 15:20    35  ****************
 235    2021-05-02 15:30    36  *****************
 236    2021-05-02 15:40    35  ****************
 237    2021-05-02 15:50    35  ****************
 238    2021-05-02 16:00    35  ****************
 239    2021-05-02 16:10    36  *****************
 ...    ..( 17 skipped).    ..  *****************
 257    2021-05-02 19:10    36  *****************
 258    2021-05-02 19:20    35  ****************
 ...    ..( 21 skipped).    ..  ****************
 280    2021-05-02 23:00    35  ****************
 281    2021-05-02 23:10    36  *****************
 282    2021-05-02 23:20    35  ****************
 ...    ..(  3 skipped).    ..  ****************
 286    2021-05-03 00:00    35  ****************
 287    2021-05-03 00:10    36  *****************
 288    2021-05-03 00:20    36  *****************
 289    2021-05-03 00:30     ?  -
 290    2021-05-03 00:40     ?  -
 291    2021-05-03 00:50     ?  -
 292    2021-05-03 01:00    28  *********

SCT Error Recovery Control command not supported

Device Statistics (GP Log 0x04)
Page  Offset Size        Value Flags Description
0x01  =====  =               =  ===  == General Statistics (rev 2) ==
0x01  0x008  4              64  ---  Lifetime Power-On Resets
0x01  0x010  4            9172  ---  Power-on Hours
0x01  0x018  6      9733955545  ---  Logical Sectors Written
0x01  0x028  6       886418554  ---  Logical Sectors Read
0x04  =====  =               =  ===  == General Errors Statistics (rev 1) ==
0x04  0x008  4               0  ---  Number of Reported Uncorrectable Errors
0x04  0x010  4               0  ---  Resets Between Cmd Acceptance and Completion
0x05  =====  =               =  ===  == Temperature Statistics (rev 1) ==
0x05  0x008  1              29  ---  Current Temperature
0x05  0x010  1              35  ---  Average Short Term Temperature
0x05  0x018  1              34  ---  Average Long Term Temperature
0x05  0x020  1              48  ---  Highest Temperature
0x05  0x028  1              24  ---  Lowest Temperature
0x05  0x030  1              40  ---  Highest Average Short Term Temperature
0x05  0x038  1              26  ---  Lowest Average Short Term Temperature
0x05  0x040  1              34  ---  Highest Average Long Term Temperature
0x05  0x048  1              29  ---  Lowest Average Long Term Temperature
0x05  0x050  4               0  ---  Time in Over-Temperature
0x05  0x058  1              65  ---  Specified Maximum Operating Temperature
0x05  0x060  4               0  ---  Time in Under-Temperature
0x05  0x068  1               0  ---  Specified Minimum Operating Temperature
0x06  =====  =               =  ===  == Transport Statistics (rev 1) ==
0x06  0x008  4             106  ---  Number of Hardware Resets
0x06  0x010  4              11  ---  Number of ASR Events
0x06  0x018  4               0  ---  Number of Interface CRC Errors
0x07  =====  =               =  ===  == Solid State Device Statistics (rev 1) ==
0x07  0x008  1               0  ---  Percentage Used Endurance Indicator
                                |||_ C monitored condition met
                                ||__ D supports DSN
                                |___ N normalized value

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0008  2            0  Device-to-host non-data FIS retries
0x0009  2            1  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x000f  2            0  R_ERR response for host-to-device data FIS, CRC
0x0010  2            0  R_ERR response for host-to-device data FIS, non-CRC
0x0012  2            0  R_ERR response for host-to-device non-data FIS, CRC
0x0013  2            0  R_ERR response for host-to-device non-data FIS, non-CRC
0x0002  2            0  R_ERR response for data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS

